%\documentclass[10pt,twocolumn]{article}
\documentclass{sigchi}
\usepackage{graphicx}
\usepackage{url}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{enumerate}

\renewcommand{\dbltopfraction}{.9}
\renewcommand{\topfraction}{.9}
\renewcommand{\textfraction}{.1}
\renewcommand{\floatpagefraction}{.9}
\renewcommand{\dblfloatpagefraction}{.9}
\newcommand{\thickhline}{\noalign{\hrule height 1.0pt}}


\def\colfigwidth{3.2in}

\def\note#1{$\rightarrow$ #1 $\leftarrow$}

%\makeatletter

%\long\def\unmarkedfootnote#1{{\long\def\@makefntext##1{##1}\footnotetext{#1}}}
%\makeatother


\renewcommand{\dbltopfraction}{.9}
\renewcommand{\topfraction}{.9}
\renewcommand{\textfraction}{.1}
\renewcommand{\floatpagefraction}{.9}
\renewcommand{\dblfloatpagefraction}{.9}

\newcommand{\code}[1]{\texttt{#1}}


\setlength{\floatsep}{0.1in}
\setlength{\textfloatsep}{0.1in}
\setlength{\dblfloatsep}{0.1in}
\setlength{\dbltextfloatsep}{0.1in}


\begin{document}

\title{Pairing Natural Interfaces with Pair Programming}
\author{Brian Kocoloski \hspace{0.3in} Cha Li  \\ 
\{briankoco,chavli\}@cs.pitt.edu \\
CS 2610: Final Project Report \\
December 14, 2012}



\maketitle

\cite{vogel} \cite{williams} \cite{konig} \cite{openni}

\begin{abstract}
It is difficult even for expert programmers to understand sufficiently large
code bases. While contemporary IDEs allow programmers to efficiently edit source
files and build and debug projects, much of a project's semantic information,
such as logical file dependencies, is not communicated particularly well in
these environments. Alternatively, in order to better understand such large
projects, users often partake in code reviews, such as pair-programming sessions
in which two programmers work together at one workstation. In this work, we
propose, implement, and evaluate a natural user interface (NUI) that allow users
to participate in pair-programming sessions with one key distinction. Rather
than operating the same terminal, one user may assume the typical role of
modifying source files using the standard keyboard and mouse interface, while
the other user is free to navigate the code base through use of our NUI. Our
preliminary analysis shows that users are able to perform some representative
tasks more quickly using our NUI than without it.

\end{abstract}

\section{Introduction}

Understanding and building accurate mental models of large (1000's of lines of code) 
software projects is a
difficult task even for very experienced programmers. The effort required by a 
developer to familiarize themselves with projects often accounts for a significant 
portion of the time allotted to the project. This situation is made worse when software
companies hire new developers (entry-level or experienced) midway through a project
or after a project to maintain it. Newly hired developers often have the required technical
skills needed for a project but are frequently faced with the daunting task of understanding 
the logical structure, custom data types, and file organization of a large project.

\subsection{Background}
Many solutions~\cite{something} have been created to address the problem of transferring knowledge to
new developers and for presenting large projects in an organized, concise, and 
understandable fashion. Contemporary integrated development environments 
(IDEs) use hierarchy displays, tabbed windows, and advanced search features to
help the user manage and display files with a project. IDEs excel at visualizing 
the physical organization (folders, files, etc) of projects, but they fail to capture much
of the semantic information available in these projects, such as file
dependencies. Furthermore, while many IDEs include rich tools for text editing,
such as find and replace mechanisms, as well as auto-complete features and
debuggers, they suffer from the inability to display the logical dependencies
between files. Like all technology, IDE's will continue to improve but they are 
unlikely to shift their focus from code development to code maintainance. 

Companies and individuals have created new programming 
paradigms and techniques to address the problem of maintaining large projects
and helping developers quickly build accurate mental models. Some examples
include agile development~\cite{blah}, the waterfall method~\cite{blah}, and regular code reviews.
All of these strategies aim to improve the overall quality of the software and
to quickly resolve bugs. One popular technique that has emerged is pair programming~\cite{williams}.
Pair programming is a technique where two programmers sit infront of a computer
and work together on the same project. The idea is that two heads are better than one. In
this technique, one developer is the ``navigator" and the other developer is the ``observer".
The navigator controls the keyboard and mouse and makes changes to the code while 
the observer watches and looks for typos, syntax errors, and logic errors. Studies
have shown the benefits of pair programming including higher quality code especially
in Computer Science education~\cite{blah2}.

Among all the benefits introduced by pair programming, it suffers from several 
weaknesses which are counter-productive. The first is the input bottleneck enforced 
by a single monitor, keyboard, and mouse. This configuration serializes any interaction 
between the developers and computer by only allowing one active task to be 
performed at a given time. Secondly, as a result of the first problem, the two developers
are relegated to ``active" and ``passive" roles. The former is the navigator and the latter
is the observer. In the worst case, the observer becomes so passive as to be unnecessary,
defeating the purpose of pair programming. A third problem is that once each developer
settles into their role, they cannot change roles without physically rearranging themselves. 

\section{Overview}
In this work, we present an extension to the pair programming model by
introducing the use of a natural user interface (NUI) for interacting with the code.
Our extension improves the traditional pair-programming environment in one key way. 
Rather than having both users sit at same workstation during the entire coding process, 
the observer is free to explore the project using hand gestures previously used
to grab the navigators attention.

NUI's offer several advantages which work well in the pair programming domain. 
Larger screensizes make reading and interacting with code easier as more code
can be displayed in a less cluttered way. Natural interaction modalities have been 
shown~\cite{something} to scale better than traditional input methods to larger displays.
Additionally, by decoupling the observer from the navigator for interaction we 
allow both developers to play active roles and allow each developer to easily
switch between active and passive modes. Our goal is to use NUI's to \textit{compliment}
existing pair programming techniques and take advantage of the strengths offered
by both. Preliminary analysis of our model has shown that we have achieved these
goals to some degree.

\section{Implementation}

\subsection{Hardware and Software}
The key piece of hardware used in our setup was a Microsoft Kinect sensor. This provided
us with all the sensory information needed to implement direct-pointing and hand gestures. 
All the data provided by the sensor was processed by a laptop with an Intel i5 CPU and 4GB of
RAM running Ubuntu Linux. Although we used a Kinect in our setup, this speficic sensor is not a 
requirement as we explain later. 

Since our target domain is using NUI's for development purposes, we opted to base our system
on Linux rather than Microsoft Windows. This was done to leverage the authors'
experience developing Linux filesystem tools, but could easily be ported to a
Windows environment in the future. As a result, we used the OpenNI framework~\cite{lib:openni}
instead of the official Kinect SDK provided by Microsoft. We lost some functionality such as
reading data from the microphones but OpenNI allows our system to be used on any operating
system using any compatible sensor device.

We developed our system in C++ using OpenGL and GLUI~\cite{lib:glui} to create
the interface and the Armadillo linear algebra library~\cite{lib:arma} to
perform all the calculations for accurate pointing and gesturing. We made some
modifications to GLUI to allow events, such as mouse clicks and key presses, to
be simulated by our interface.

\subsection{Pointing and Gesturing}
In existing pair programming situations, the passive developer watches for mistakes that go unnoticed
by the active developer. When the passive user needs to notify the active user of some event, it is 
natural to point to or gesture towards an area of the monitor that requires the active user's attention.
Additionally, gestures can also be used to request an action to be performed such as waving your hand
to indicate ``no" or ``cancel".

\subsubsection{Interaction Modes}
Our NUI takes these natural interactions and augments them using software to perform tasks 
that physical pointing and gesturing alone cannot perform. Metaphorically speaking, we 
aimed to create an ``enhanced laser pointer". Our system supports single- and dual-handed 
absolute pointing, single- and dual-handed selection and simple gestures.

Absolute pointing, as defined by Konig et. al ~\cite{konig}, is the most natural finger-screen
mapping as it does not consider velocity or acceleration when placing the cursor. The cursor,
ideally, shows up exactly where the user is pointing and expecting.

There have been many techniques used for selection in pointing interfaces. Our single-handed
selection technique imitates a commonly used gesture for selection in real life: pointing,
rotating your arm along its length, and gesturing to ``come over"  with your finger. In our case,
we ignore the gesturing and allow selection by pointing and rotating.

Since rotating an arm causes it to move along other axes, more precise selection is done using 
two arms. The right arm is used to point while the left arm is indicates selection through 
swiping gestures. Additional techniques to improve accuracy and precision are discussed in
the next section.

We also use gestures to perform other basic file manipulation operations. Swiping ``right" and ``left"
opens and closes files respectively while swiping ``up" and ``down" scrolls through files.

\subsubsection{Modelling Users}
To faciliate pointing and gesturing, our system models a user by measuring various locations on 
their body. Borrowing geographical terms, our system calculates the ``equator"  and ``meridians"
of the user. 
$$ insert\_diagram $$
\subsubsection{Accuracy and Precision}
Accuracy and precision are big issues when it comes to pointing interfaces. The human body has
a natural jitter~\cite{konig} when it has to maintain a position for an extended period of time. This
jitter creates noise in the pointing data which hurts both accuracy and precision. Many noise
cancelling techniques have been used such as low-pass filters and Kalman filters~\cite{something}
to improve accuracy. Our system uses a multivariate gaussian to model pointing behavior and remove
noisy data. 
$$ insert\_equation $$
$\mathbf{x}$ is a 3 element feature vector describing a ``pointing" action using pitch, yaw, depth.

Our system maintains pointing accuracy even while the user is moving and poitning. The simple case
is when the user moves left or right, this is simply a linear transformation for the cursor. Moving towards
or away from the screen is a non-linear relationship so must be modelled differently. Intuitively, 
as you move away from a screen the adjustments you make to maintain a constant ``pointing" become
smaller. Conversely, as you move close to the screen you will find yourself moving your arm more
to maintain a constant ``pointing". In otherwords, the effect of distance on pointing angle becomes
smaller at larger distances. We model this by using the power law, more spefically we use the following
relationship:
$$ insert\_equation $$


\subsection{File Navigation}

In order to make our interface more palatable for programming exercises, we
supplemented our graphical user interface with backend tools useful for various
programming tasks. In particular, the graphical interface had two important
features to aid programming tasks: (1) a file browser, and (2) an textbox to
display and interact with file contents.

A file browser is useful because it allows users to fluidly navigate through a
source tree and perform actions such as opening files and changing directories.
Although we also provided a much richer mechanism for navigating through areas
of the source tree in a context-specific manner, the file browser allows users
to accomplish the same basic tasks that they are familiar with in standard
desktop environments. Because of this, we feel that users may more quickly
adjust to using our interface than they otherwise might in the absence of a
standard file browsing mechanism.

Because, for very large projects, manually navigating through a source tree can
be a confusing and fruitless task, we also implemented a data definition
searching tool. When a user opens in a file by selecting it in the file browser,
the file's contents are displayed in the interactive file textbox. If, while the
user is browsing the file's contents using the aforementioned swipe and pointing
features, they come across a data type that they do not understand, they can
point at any line of code where the type is used and perform one of the swiping
gestures. This triggers the backend of the interface to perform a search for the
definition of the data type that the user has requested. If the search proves
successful, the file browser automatically navigates to the directory where the
file defining the user-requested data type exists, and the file's contents are
loaded into the file browser. At any point in time, a user can perform a gesture
to return to the previously loaded file.

\section{Experimental Evluation}

We will now describe our experimental evaluation. The goal of this evaluation
was to determine if use of our natural interface could aid users in
accomplishing various programming tasks. The nature of these tasks and the
methodology behind our evaluation will be described in this section.

\subsection{Tasks}

For our evaluation, we created three separate programming tasks for users to
accomplish.

\begin{enumerate}[(A)]
    \item Locate the file where a data type is defined in the Linux kernel.
    Users were given two data types, and the time until the files defining these
    data types were found was recorded.
    \item Find and fix two bugs in a project previously developed by
    one of the authors. These tasks required users to include missing header
    files in various source files. The time until users successfully compiled
    the project (using a pre-written \code{Makefile}) was recorded.
    \item Write a small (5 to 10 LOC) function to add some small functionality
    to a given project. The time until users could successfully write this
    function was recorded. The code produced output, and the output was checked
    against a solution with a script.  The time until users could successfully
    write this function was recorded.
\end{enumerate}

For task A, users were given the source code for the 2.6.40 version of the
Linux kernel. For tasks B and C, the existing projects each consisted of
over 15,000 LOC.

\subsection{Methodology}

For our experiments, we recruited 5 groups of 2 users each from the Computer
Science department at our university. We performed a within-user study in which
each group of users completed all tasks. Each group was responsible for
completing two versions of tasks A, B, and C described above. For one
version of each task, one user from the group used our interface, while the
other operated the terminal and made any required changes to the source files.
For the other version, the users sat at the same terminal and reviewed code as
in the typical pair programming model. Each version of a given task was
different from the other version, but they were of roughly equal difficulty. For
each group, the order of these 6 tasks was randomized.

While these tasks are clearly not representative of all programming tasks, they
nevertheless represent a workload that is characteristic of many tasks that
newly hired programmers may be presented with. None of the users in our study
claimed to be familiar with any of the source code that they were presented
with, so we believe that our experiments presented users with a workload
representative of what they might experience in realistic situations.

\subsection{Results}

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.7]{figures/results}
    \end{center}
    \caption{Execution times for each task}
    \label{fig:results}
\end{figure}

The results of our experiments can be seen in Figure~\ref{fig:results}. As can
be clearly seen, tasks A and B were easier for users to perform with the
natural interface than without it. Analysis of variance (ANOVA) showed a
significant effect for using the natural interface for tasks A (F$_{1, 4} = 11.192,
p < 0.05$) and B (F$_{1, 4} = 33.768, p < 0.05$). Users commented that, for
these tasks, the file browsing mechanisms present in the natural interface were
extremely helpful, and that the ability to quickly navigate between files found
when searching for a data type, and previous files that were loaded into the
interactive textbox when the search was performed was helpful.

For task C, the results were not as positive. ANOVA showed no significant effect
for using the interface (F$_{1, 4} = 0.96, p = 0.43$). In this case, we noticed
that users spent much more time working together on logical problems that on
browsing through the source code. As such, this is not an entirely unexpected
result. The natural interface does not provide much benefit when the tasks being
performed are of a more logically intense nature. We note, however, that using
the natural interface does not put users at a disadvantage in this case. Users
were still able to communicate effectively, just as they were in the standard
pair-programming experiments.

\section{Discussion and Conclusion}

The results of our experiments are largely encouraging. The tasks that were
shown to be significantly easier to perform when using our natural interface are
common tasks that programmers working on new projects may be faced with. While
part of the reason that the natural interface was successful was due to the file
searching tools that we developed, some users claimed that using the natural
interface allowed them to more fluidly navigate the source tree than they could
without it.

We feel that we have only "scratched the surface" of the capabilities of a natural
interface for solving some of these tasks. In particular, we feel that using the
Kinect's speech recognition capability could prove extremely beneficial in the
future, as performing some of the gestures currently required to use the
interface are not very easy to do. Furthermore, we believe that displaying
multiple files simultaneously and allowing users to visualize their
dependencies, both in terms of how they are physically arranged in a source
tree and in terms of how they logically depend on each other, would make the
interface even more effective for the tasks that we have identified.

With that said, our current implementation proved superior to the standard
pair-programming model for two of the three tasks that we presented users with,
and it did not negatively impact them for the third. We believe that our work
has sufficiently shown the potential for natural interfaces to augment the task
of reviewing code through pair-programming.


\bibliographystyle{acm}
\bibliography{hci-final}
\end{document}
