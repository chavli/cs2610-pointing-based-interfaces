#include "controls/uicontrol.h"
#include <iostream>

int main(int argc, char **argv){
  initGuiControl(&argc, argv);
  startGuiControl();

  return 0;
}
