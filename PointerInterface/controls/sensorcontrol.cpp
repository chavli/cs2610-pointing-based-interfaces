
#include "sensorcontrol.h"
#include "usercontrol.h"
#include "../math/xnscalarmath.h"
#include "../math/xnvectormath.h"
#include "../math/gaussianset.h"
#include <iostream>
#include <deque>
#include <stdint.h>

using namespace xn;
using namespace std;

#define DEBUG

/******************************************************************************
* "private" variables and function prototypes
******************************************************************************/
Context context;
const int MAX_USERS = 1;

//callback function used to send opengl messages
void (*sendMessage)(message_t *) = 0;

//callback handles
XnCallbackHandle usercb_handle;
XnCallbackHandle skelstart_handle;
XnCallbackHandle skelprocess_handle;
XnCallbackHandle skelend_handle;

//user and skeleton data
UserGenerator user_gen;
std::set<UserControl *> tracked_users;
int num_users = 0;

//left hand gestures
int gesture_length = 30;
deque<XnVector3D> gesture_buf;
int swipe_max = 1000 , swipe_min = 100;
XnBool swipe_reset = false;
swipedir_t swipe_dir = SWIPEDIR_NONE;

//calibration samples for pointing
XnUInt32 required_samples = 200;
target_t calibration_target = TG_NONE;
XnBool sensor_calibrated = false;

//determines the angle and extension the arm has to have to be considered
//"pointing"
XnFloat ext_threshold = 400;
XnFloat deg_threshold = 50;

//model pointing history as a multivar gaussian this is used for pointer 
//stabilization.
const int D = 3;
//(dimensions, num samples, update freq, upper thresh, lower thresh)
GaussianSet rh_gaussian(D, 100, 20, .95f, .35f);
GaussianSet lh_gaussian(D, 100, 20, .95f, .35f);
XnVector3D last_rtarget, last_ltarget;
float last_rarmext, last_rpitch, last_ryaw;
float last_larmext, last_lpitch, last_lyaw;
bool notify_rpoint = false;
bool notify_lpoint = false;

//used for selection detection
const unsigned int N = 10;
const float select_thresh = 0.075;
deque<float> r_seldata;
deque<float> l_seldata;
bool r_selecting = false;
bool l_selecting = false;
//prevents sensor from spamming ui with selection messages
bool notify_rselect = false; 
bool notify_lselect = false; 

//buffer for printing
char out[512];

//outgoing message to ui
message_t *to_send;

FILE *fout;


static inline XnBool IS_POINTING(XnFloat ext, XnFloat deg){
  return (ext >= ext_threshold); //&& (deg <= deg_threshold);
}

//rotation is the value in [joint].orientation.elements[4]
static inline XnBool IS_SELECTING(XnFloat rotation){
  return abs( rotation ) >= select_thresh;
}

static inline XnBool IS_SWIPING(XnFloat distance){
  return (abs(distance) <= swipe_max) && (abs(distance) > swipe_min);
}

//callback functions to handle events from the kinect sensor
void XN_CALLBACK_TYPE onNewUser(UserGenerator&, XnUserID, void *);
void XN_CALLBACK_TYPE onLostUser(UserGenerator&, XnUserID, void *);
void XN_CALLBACK_TYPE onStartSkeletonCalibrate(SkeletonCapability&, XnUserID, void *);
void XN_CALLBACK_TYPE onProcessSkeletonCalibrate(SkeletonCapability&, XnUserID, XnCalibrationStatus, void *);
void XN_CALLBACK_TYPE onEndSkeletonCalibrate(SkeletonCapability&, XnUserID, XnCalibrationStatus, void *);

//some wrapper functions to make sending messages back to the ui easier
void uiprint(float x, float y, char *);
void uiprintNotification(const char *);

/******************************************************************************
* general control
******************************************************************************/
void initSensorControl(int *argc, char **argv){
  fout = fopen("data.csv", "w");

  XnStatus result;
  result = context.Init();
  CHECK_RC(result, "Failed to Initialize Context");

  result = user_gen.Create(context);
  CHECK_RC(result, "Failed to Initilize User Generator");
  
  user_gen.RegisterUserCallbacks(
    onNewUser,
    onLostUser,
    NULL,
    usercb_handle);

  result = context.StartGeneratingAll();
  CHECK_RC(result, "Initialize Data");
}

void startSensorControl(){
  std::cout << "Setting Skeleton Callbacks\n";
  SkeletonCapability skel_data = user_gen.GetSkeletonCap();
  //setup skeleton callbacks
  skel_data.RegisterToCalibrationStart(onStartSkeletonCalibrate,
    NULL,
    skelstart_handle
  );
  skel_data.RegisterToCalibrationInProgress(onProcessSkeletonCalibrate,
    NULL,
    skelprocess_handle
  );
  skel_data.RegisterToCalibrationComplete(onEndSkeletonCalibrate,
    NULL,
    skelend_handle
  );
  skel_data.SetSmoothing(0.85);
  
  //XN_SKEL_PROFILE_UPPER /ALL
  //tell sensor to monitor all joints
  skel_data.SetSkeletonProfile(XN_SKEL_PROFILE_ALL);
  std::cout << "Done Initializing Skeleton\nStarting Program\n";
}

void endSensorControl(){
  user_gen.UnregisterUserCallbacks(usercb_handle);
  user_gen.Release();
  context.StopGeneratingAll();
  context.Release();

  if( fout )
    fclose(fout);
}

void sensorControlMessageHandler( void (*handler)(message_t *)){
  sendMessage = handler; 
}

/******************************************************************************
* main part of the program. handles reading sensors and updating the 
* screen
******************************************************************************/
int sample_rate = 10;
int s = 0;
void updateSensor(void){
  // ---- BEGIN READING SENSOR 
  context.StartGeneratingAll();
  SkeletonCapability skel_data = user_gen.GetSkeletonCap();
  
  //handle skeleton data
  context.WaitOneUpdateAll(user_gen);
 
  // ---- END READING SENSOR
  
  if( tracked_users.size() == 0 )
    uiprintNotification("No Users Detected! Someone move around!");

  //send message to UI to draw a target
  if(calibration_target != TG_NONE){
    to_send = (message_t *)malloc(sizeof(message_t));
    glshape_data_t *data = (glshape_data_t *)malloc(sizeof(glshape_data_t));

    switch(calibration_target){
      case TG_TOP_LEFT:
        data->x1 = -1.0f; data->y1 = 1.0f; data->x2 = -.85f; data->y2 = .85f;
        data->r = 1.0f; data->g = 0.0f; data->b = 0.0f;
        break;
      case TG_BOTTOM_RIGHT:
        data->x1 = .85f; data->y1 = -.85f; data->x2 = 1.0f; data->y2 = -1.0f;
        data->r = 1.0f; data->g = 0.0f; data->b = 0.0f;
        break;
      case TG_CENTER:
        data->x1 = -.01f; data->y1 = .01f; data->x2 = .01f; data->y2 = -.01f;
        data->r = 1.0f; data->g = 0.0f; data->b = 0.0f;
      default:
        break;
    }
    
    if( sendMessage ){
      to_send->type = DRAW_RECT; to_send->fid = 0; to_send->p_data = data;
      (*sendMessage)(to_send);
    }
    else{
      free(data);
      free(to_send);
    }
  }

  XnFloat x_offset, arm_ext, pitch, yaw;
  std::set<UserControl *>::iterator it;
  
  XnSkeletonJointTransformation torso, rhand, lhand, rshoulder, lshoulder, relbow, lelbow;
  XnSkeletonJointPosition torso_p, rhand_p, lhand_p, rshoulder_p, lshoulder_p;
  XnSkeletonJointOrientation relbow_o, lelbow_o;


  //calibrate pointer settings for users
  if((s++ % sample_rate) == 0 ){
    s = 0;
    
    for( it=tracked_users.begin(); it != tracked_users.end(); it++ ){
      if( !(*it)->isCalibrated() && skel_data.IsTracking((*it)->getUserID() )){
        skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_TORSO, torso);
        skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_RIGHT_SHOULDER, lshoulder);
        skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_LEFT_SHOULDER, rshoulder);
        skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_RIGHT_HAND, lhand);
        skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_LEFT_HAND, rhand);
        
        //extract position data from transform data
        torso_p = torso.position; lshoulder_p = lshoulder.position; 
        rshoulder_p = rshoulder.position; lhand_p = lhand.position;
        rhand_p = rhand.position;


        float c = (rhand_p.fConfidence + lhand_p.fConfidence + torso_p.fConfidence) / 3.0;
        if( c > .75 ){ 
          XnFloat delta_y, delta_x;
          x_offset = torso_p.position.X - (*it)->getCenter();
          delta_y = (*it)->getRightEquator() - rhand_p.position.Y;
          delta_x = (*it)->getRightMeridian() - (rhand_p.position.X - x_offset );

          arm_ext = torso_p.position.Z - rhand_p.position.Z;
          
          pitch = atan( abs(delta_y) / arm_ext );
          yaw = atan( abs(delta_x) / arm_ext );

          memset(out, 0, sizeof(out)); 

          sprintf(out, "Confidence: %.2f, X Offset: %.2f, Arm Extension: %.2f -- Pitch(D): %.2f, Yaw(D): %.2f", c , x_offset, arm_ext, radToDeg(pitch), radToDeg(yaw));
          uiprint(.1f, .2f, out);

          //NOTE: calibration is done with right hand. we can change this later

          switch( (*it)->getCalibrationState() ){
            case INIT_STANCE:
              if( (*it)->calibrateStance(torso_p.position, rshoulder_p.position, lshoulder_p.position, rhand_p.position, lhand_p.position) ){
                (*it)->finalizeStance();
                (*it)->setCalibrationState(FIRST_TARGET);
                std::cout << **it << "\n";
              }
              break;
            case FIRST_TARGET:{
                calibration_target = TG_TOP_LEFT;

                if( IS_POINTING( arm_ext, radToDeg(pitch) )){
                  XnVector3D *extend = extendv(pitch, yaw, rhand_p.position.Z);
                  if( delta_y > 0 ){ extend->Y *= -1; }
                  if( delta_x > 0 ){ extend->X *= -1; }
                  
                  XnVector3D *target = addvv(&(rhand_p.position), extend);
                  
                  memset(out, 0, sizeof(out)); 
                  sprintf(out, "X: %.2f, Y: %.2f -- Pitch(D): %.2f, Yaw(D): %.2f", target->X, target->Y, radToDeg(pitch), radToDeg(yaw));
                  uiprint(.1f, .15f, out);

                  if( (*it)->calibrateTarget( target->X, target->Y ) ){
                    (*it)->setCalibrationState(SECOND_TARGET);
                    std::cout << **it << "\n";
                  }
                  free(extend);
                  free(target);
                  std::cout << (*it)->getNeededSamples() << "\n";
                }
              }
              break;
            case SECOND_TARGET:{
                calibration_target = TG_BOTTOM_RIGHT;
                
                if( IS_POINTING( arm_ext, radToDeg(pitch) )){
                  XnVector3D *extend = extendv(pitch, yaw, rhand_p.position.Z);
                  if( delta_y > 0 ){ extend->Y *= -1; }
                  if( delta_x > 0 ){ extend->X *= -1; }
                  
                  XnVector3D *target = addvv(&(rhand_p.position), extend); 
                  
                  memset(out, 0, sizeof(out)); 
                  sprintf(out, "X: %.2f, Y: %.2f -- Pitch(D): %.2f, Yaw(D): %.2f", target->X, target->Y, radToDeg(pitch), radToDeg(yaw));
                  uiprint(.1f, .15f, out);

                  if( (*it)->calibrateTarget( target->X, target->Y ) ){
                    (*it)->finalizeTargets();
                    (*it)->setCalibrationState(USER_CALIBRATED);
                    
                    if( sendMessage ){
                      XnVector3D tl, br;
                      (*it)->getTopLeft(&tl);
                      (*it)->getBottomRight(&br);

                      to_send = (message_t *)malloc(sizeof(message_t));
                      glshape_data_t *data = (glshape_data_t *)malloc(sizeof(glshape_data_t));
                      data->x1 = tl.X; data->y1 = tl.Y; 
                      data->x2 = br.X; data->y2 = br.Y; 

                      to_send->type = SET_DIM;
                      to_send->p_data = data;
                      
                      (*sendMessage)(to_send);
                      sensor_calibrated = true;
                    }
                     
                    std::cout << **it << "\n";
                    
                    calibration_target = TG_CENTER;
                  }
                  free(extend);
                  free(target);
                  std::cout << (*it)->getNeededSamples() << "\n";
                }
              }
              break;
            default:
              break;
          }
        } // ---- end confidence check
        else{
          uiprintNotification("Please Wiggle. I'm not sure you're there...");
        }
      }
    } // ---- end calibration stuff
  } 
  //determine if any users are pointing
  for( it=tracked_users.begin(); it != tracked_users.end(); it++ ){
    if( (*it)->isCalibrated() && skel_data.IsTracking((*it)->getUserID() )){
      skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_TORSO, torso);
      skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_LEFT_HAND, rhand);
      skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_RIGHT_HAND, lhand);
      skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_LEFT_ELBOW, relbow);
      skel_data.GetSkeletonJoint( (*it)->getUserID(), XN_SKEL_RIGHT_ELBOW, lelbow);
      
      //extract position data
      torso_p = torso.position; rhand_p = rhand.position; lhand_p = lhand.position;
      relbow_o = relbow.orientation; lelbow_o = lelbow.orientation;

      //determine if the user has moved left/right of their original position
      x_offset = torso_p.position.X - (*it)->getCenter();
      
      //use the power law to reduce the effect of z-distance(depth) on angle at
      //larger distances. instead of using the users actual depth data
      //we calcuate a "virtual" depth, d', that is used to calcuate angles
      //
      //  d' = w(cur_dist) * delta_d + init_dist
      //
      //  where delta_d = cur_dist - init_dist, and 
      //  w(x) = 1 - log10( log10(x) + 1 )
      
      XnFloat delta_z = torso_p.position.Z - (*it)->getInitialZ(); 
      delta_z = (1 - log10( log10(torso_p.position.Z) + 1)) * delta_z;

      //-- BEGIN RIGHT HAND
      XnFloat delta_y = (*it)->getRightEquator() - rhand_p.position.Y;
      XnFloat delta_x = (*it)->getRightMeridian() - (rhand_p.position.X - x_offset );
      
      float conf = (rhand_p.fConfidence + torso_p.fConfidence) / 2;

      if( conf > .75 ){
        arm_ext = torso_p.position.Z - rhand_p.position.Z;
        pitch = atan( abs(delta_y) / arm_ext );
        yaw = atan( abs(delta_x) / arm_ext );
        
        //create a new data sample for the gaussian model
        fmat sample = zeros<fmat>(0, D);
        sample << pitch << yaw << arm_ext << endr;
        
        if(!rh_gaussian.isParameterized() || rh_gaussian.isSimilar(sample) ||
            rh_gaussian.isOutlier(sample)){
            last_rarmext = arm_ext;
            last_rpitch = pitch;
            last_ryaw = yaw;
        }

        if( IS_POINTING(last_rarmext, radToDeg(last_rpitch)) ){
          float v_dist = (*it)->getInitialZ() + delta_z; //users virtual distance
          XnVector3D *extend = extendv(last_rpitch, last_ryaw, v_dist - arm_ext);
          
          if( delta_y > 0 ){ extend->Y *= -1; }
          if( delta_x > 0 ){ extend->X *= -1; }
          XnVector3D *target = addvv(&(rhand_p.position), extend);
          
          memset(out, 0, sizeof(out)); 
          sprintf(out, "X-Offset: %.2f, X: %.2f, Y: %.2f -- Pitch(D): %.2f, Yaw(D): %.2f",
            x_offset, target->X, target->Y,  radToDeg(last_rpitch), radToDeg(last_ryaw));
          uiprint(.1f, .15f, out);

          memset(out, 0, sizeof(out));
          sprintf(out, "Arm Ext: %.2f, Used Arm Ext: %.2f, p = %.2f", arm_ext, last_rarmext, rh_gaussian.similarity(sample, true));
          uiprint(.1f, .1f, out);

          //ignore noise
          if(rh_gaussian.isOutlier(sample) || rh_gaussian.isSimilar(sample)){
            rh_gaussian.add(sample);
            last_rtarget.X = target->X; 
            last_rtarget.Y = target->Y;
          } 
          
          //check if right arm selecting
          float rot = relbow_o.orientation.elements[3];
          if( r_seldata.size() < N )
            r_seldata.push_back(rot);
          else{
            float delta = rot - r_seldata.front();
            if( IS_SELECTING( delta ) ){
              r_seldata.clear();
              r_seldata.push_back(rot);
              r_selecting = ( delta < 0 ) ? true : false;
              notify_rselect = r_selecting;
            }
            else{
              r_seldata.pop_front();
              r_seldata.push_back(rot);
            }
          }
          free(extend);
          free(target);
        } //-- end POINT check
        else{
          r_selecting = false;
          notify_rselect = false;
          notify_rpoint = false;
          r_seldata.clear();
          rh_gaussian.reset();
        }
      }
      //-- END RIGHT HAND

      //-- BEGIN LEFT HAND
      x_offset = torso_p.position.X - (*it)->getCenter();
      float leftarm_offset = (*it)->getLeftMeridian() - (*it)->getRightMeridian();
      delta_y = (*it)->getLeftEquator() - lhand_p.position.Y;
      delta_x = (*it)->getCenter() - (lhand_p.position.X - x_offset - (leftarm_offset/ 2));

      conf = (lhand_p.fConfidence + torso_p.fConfidence) / 2;

      if( conf > .75 ){
        arm_ext = torso_p.position.Z - lhand_p.position.Z;

        pitch = atan( abs(delta_y) / arm_ext);
        yaw = atan( abs(delta_x) / arm_ext );
        
        //create a new data sample for the gaussian model
        fmat sample = zeros<fmat>(0, D);
        sample << pitch << yaw << arm_ext << endr;
        
        if(!lh_gaussian.isParameterized() || lh_gaussian.isSimilar(sample) ||
            lh_gaussian.isOutlier(sample)){
            last_larmext = arm_ext;
            last_lpitch = pitch;
            last_lyaw = yaw;
        }
        
        if( IS_POINTING(last_larmext, radToDeg(last_lpitch)) ){
          if( gesture_buf.size() < gesture_length )
            gesture_buf.push_back(lhand_p.position);
          else{
            gesture_buf.pop_front();
            gesture_buf.push_back(lhand_p.position);
            
            //left/right swipe takes precedence over up/down
            float swipe1 = lhand_p.position.X - gesture_buf.front().X;
            if( !swipe_reset && IS_SWIPING(swipe1)){
              swipe_dir = (swipe1 > 0) ? SWIPEDIR_LEFT : SWIPEDIR_RIGHT;
              swipe_reset = true;
            }

            float swipe2 = lhand_p.position.Y - gesture_buf.front().Y;
            if( !swipe_reset && IS_SWIPING(.7 * swipe2)){
              swipe_dir = (swipe2 > 0) ? SWIPEDIR_UP : SWIPEDIR_DOWN;
              swipe_reset = true;
            }
            
            //cout << swipe1 << " " << swipe2 << endl;

            //fprintf(fout, "%.2f, %.2f\n", swipe1, swipe2);
          }
          
          float v_dist = (*it)->getInitialZ() + delta_z; //users virtual distance
          XnVector3D *extend = extendv(last_lpitch, last_lyaw, v_dist - arm_ext);
          
          if( delta_y > 0 ){ extend->Y *= -1; }
          if( delta_x > 0 ){ extend->X *= -1; }
          XnVector3D *target = addvv(&(lhand_p.position), extend);
          
          memset(out, 0, sizeof(out)); 
          sprintf(out, "Left-Offset: %.2f, X-Offset: %.2f, X: %.2f, Y: %.2f -- Pitch(D): %.2f, Yaw(D): %.2f",
            leftarm_offset, x_offset, target->X, target->Y,  radToDeg(last_lpitch), radToDeg(last_lyaw));
          uiprint(.1f, .9f, out);

          memset(out, 0, sizeof(out));
          sprintf(out, "Arm Ext: %.2f, Used Arm Ext: %.2f, p = %.2f", arm_ext, last_larmext, lh_gaussian.similarity(sample, true));
          uiprint(.1f, .85f, out);
          
          target->X += (3 * leftarm_offset);

          //ignore noise
          if(lh_gaussian.isOutlier(sample) || lh_gaussian.isSimilar(sample)){
            lh_gaussian.add(sample);
            last_ltarget.X = target->X; 
            last_ltarget.Y = target->Y;
          }

          //check if left arm selecting
          float rot = lelbow_o.orientation.elements[3];
          if( l_seldata.size() < N )
            l_seldata.push_back(rot);
          else{
            float delta = rot - l_seldata.front();
            if( IS_SELECTING( delta ) ){
              l_seldata.clear();
              l_seldata.push_back(rot);
              l_selecting = ( delta < 0 ) ? true : false;
              notify_lselect = l_selecting;
            }
            else{
              l_seldata.pop_front();
              l_seldata.push_back(rot);
            }
          }

          free(extend);
          free(target);
        } //-- end POINT check
        else{
          gesture_buf.clear();
          swipe_reset = false;
          l_selecting = false;
          notify_lselect = false;
          notify_lpoint = false;
          l_seldata.clear();
          lh_gaussian.reset();
        }
      }         
      //-- END LEFT HAND

      //-- NOTIFY UI OF EVENTS
      if( sendMessage ){
        //notify right arm pointing
        to_send = (message_t *)malloc(sizeof(message_t));
        glposition_t *data = (glposition_t *)malloc(sizeof(glposition_t));
        data->x1 = last_rtarget.X; data->y1 = last_rtarget.Y; 
        if( !r_selecting ){
          data->r = 0.0f; data->g = 0.0f; data->b=1.0f;
        }
        else{
          data->r = 0.0f; data->g = 1.0f; data->b=0.0f;
        }
        sprintf(data->s, "U%d - R", (*it)->getUserID());
        to_send->type = CURSOR_POS;
        to_send->p_data = data;
        (*sendMessage)(to_send);
        notify_rpoint = false;
        
        //notify left arm pointing
        /*
        to_send = (message_t *)malloc(sizeof(message_t));
        data = (glposition_t *)malloc(sizeof(glposition_t));
        data->x1 = last_ltarget.X; data->y1 = last_ltarget.Y;
        if( !l_selecting ){
          data->r = 0.0f; data->g = 0.0f; data->b=1.0f;
        }
        else{
          data->r = 0.0f; data->g = 1.0f; data->b=0.0f;
        }
        sprintf(data->s, "U%d - L", (*it)->getUserID());
        to_send->type = CURSOR_POS;
        to_send->p_data = data;
        (*sendMessage)(to_send);
        notify_lpoint = false;
        */

        //notify swipes (currently left arm only)
        if( swipe_dir != SWIPEDIR_NONE ){
          to_send = (message_t *)malloc(sizeof(message_t));
          data = (glposition_t *)malloc(sizeof(glposition_t));
           
          if( swipe_dir == SWIPEDIR_LEFT )
            strcpy(data->s, "Swiped Left");
          else if( swipe_dir == SWIPEDIR_RIGHT )
            strcpy(data->s, "Swiped Right");
          else if( swipe_dir == SWIPEDIR_UP )
            strcpy(data->s, "Swiped Up");
          else if( swipe_dir == SWIPEDIR_DOWN )
            strcpy(data->s, "Swiped Down");
          else
            strcpy(data->s, "Unknown Swipe");

          //send location of where right hand is pointing
          data->x1 = last_rtarget.X; data->y1 = last_rtarget.Y;
          data->r = (float)swipe_dir; //use ->r to store swipe direction 
          to_send->type = LH_SWIPE;
          to_send->p_data = data;
          (*sendMessage)(to_send);

          swipe_dir = SWIPEDIR_NONE; 
        }

        if( notify_rselect ){
          to_send = (message_t *)malloc(sizeof(message_t));
          glposition_t *data = (glposition_t *)malloc(sizeof(glposition_t));
          data->x1 = last_rtarget.X; data->y1 = last_rtarget.Y;
          to_send->type = CURSOR_SEL;
          to_send->p_data = data;
          (*sendMessage)(to_send);
          notify_rselect = false;
        }
        
        if( notify_lselect ){
          to_send = (message_t *)malloc(sizeof(message_t));
          glposition_t *data = (glposition_t *)malloc(sizeof(glposition_t));
          data->x1 = last_ltarget.X; data->y1 = last_ltarget.Y;
          to_send->type = CURSOR_SEL;
          to_send->p_data = data;
          (*sendMessage)(to_send);
          notify_lselect = false;
        } 
      }              

    }
 }   // ---- END READING SENSOR
} //---- END updateSensor()


/******************************************************************************
* kinect user callback functions
******************************************************************************/
void XN_CALLBACK_TYPE onNewUser(UserGenerator &ug, XnUserID uid, void *cookie){
  std::cout << "New User Detected -> " << uid << "\n";
  
  if(num_users++ < MAX_USERS)
    user_gen.GetSkeletonCap().RequestCalibration(uid, true);
}

void XN_CALLBACK_TYPE onLostUser(UserGenerator &ug, XnUserID uid, void *cookie){
  std::cout << "User Lost: " << uid << "\n";
  if( user_gen.GetSkeletonCap().IsCalibrating(uid) )
    user_gen.GetSkeletonCap().AbortCalibration(uid);

  if( user_gen.GetSkeletonCap().IsTracking(uid) )
    user_gen.GetSkeletonCap().StopTracking(uid);
  
  //remove the user
  std::set<UserControl *>::iterator it;
  for( it=tracked_users.begin(); it != tracked_users.end(); it++ ){
    if( (*it)->getUserID() == uid ){
      tracked_users.erase(it);
      break;
    }
  }
  num_users--;
}

/******************************************************************************
* kinect skeleton callback functions
******************************************************************************/
void XN_CALLBACK_TYPE onStartSkeletonCalibrate(
  SkeletonCapability &sc,
  XnUserID uid,
  void *cookie){
  
  std::cout << "Calibrating User " << uid << "\n";

}

void XN_CALLBACK_TYPE onProcessSkeletonCalibrate(
  SkeletonCapability &sc,
  XnUserID uid,
  XnCalibrationStatus status,
  void *cookie){
  
  if( status != XN_CALIBRATION_STATUS_OK )
    std::cout << "User " << uid << " Calibration Status: " << status << "\n";

}

void XN_CALLBACK_TYPE onEndSkeletonCalibrate(
  SkeletonCapability &sc,
  XnUserID uid,
  XnCalibrationStatus status, 
  void *cookie){
   
  if( status == XN_CALIBRATION_STATUS_OK ){
    std::cout << "Successfully Calibrated User " << uid << " to Sensor.\n";
    user_gen.GetSkeletonCap().StartTracking(uid);
    
    UserControl *new_user = new UserControl(uid, required_samples); 
    tracked_users.insert(new_user);
    
    std::cout << "Beginning User " << uid << " Pointing Calibration.\n";
  }
  else{
    std::cout << "Failed to Calibrated User " << uid << " (" << user_gen.GetNumberOfUsers() << ") \n";
    std::cout << "Trying again...\n";
    user_gen.GetSkeletonCap().RequestCalibration(uid, true);
  }
}

/******************************************************************************
* some convenience functions 
******************************************************************************/
void uiprint(float x, float y, char *str){
  #ifdef DEBUG  
    to_send = (message_t *)malloc(sizeof(message_t));
    gltext_data_t *data = (gltext_data_t *)malloc(sizeof(gltext_data_t));

    data->x1 = (sensor_calibrated) ?  .5 - (.5 - x) : x;
    data->y1 = y; 
    data->r = 0.0f; data->g = 0.0f; data->b = 0.0f;
    memcpy( data->s, str, 512 );

    if( sendMessage ){
      to_send->type = DRAW_TEXT;
      to_send->p_data = data;
      (*sendMessage)(to_send);
    }
    else{
      free(data);
      free(to_send);
    }
  #endif
}

void uiprintNotification(const char *note){
  to_send = (message_t *)malloc(sizeof(message_t));
  gltext_data_t *data = (gltext_data_t *)malloc(sizeof(gltext_data_t));

  data->x1 = (sensor_calibrated) ? .6 : .3;
  data->y1 = .5; 
  data->r = 1.0f; data->g = 0.5f; data->b = 0.0f;
  memcpy( data->s, note, 512 );

  if( sendMessage ){
    to_send->type = DRAW_TEXT;
    to_send->p_data = data;
    (*sendMessage)(to_send);
  }
  else{
    free(data);
    free(to_send);
  }

}

void uiprintOrientation(XnSkeletonJointOrientation *joint, char *name){
  memset(out, 0, sizeof(out)); 
  sprintf(out, "%s Orientation X: %.2f, %.2f, %.2f Y: %.2f, %.2f, %.2f Z: %.2f, %.2f, %.2f",
          name,
          joint->orientation.elements[0],
          joint->orientation.elements[1],
          joint->orientation.elements[2],
          joint->orientation.elements[3],
          joint->orientation.elements[4],
          joint->orientation.elements[5],
          joint->orientation.elements[6],
          joint->orientation.elements[7],
          joint->orientation.elements[8]
  );
  uiprint(.6f, .9f, out);
}
