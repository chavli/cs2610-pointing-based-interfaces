
#include "usercontrol.h"
#include "../math/xnvectormath.h"

UserControl::UserControl(XnUserID uid){
  this->uid = uid;
  
  this->cal_state = INIT_STANCE;
  this->num_samples = 100;
  this->samples_needed = 100;

  memset(&(this->init_torso), 0, sizeof(struct XnVector3D));
  memset(&(this->init_rshoulder), 0, sizeof(struct XnVector3D));
  memset(&(this->init_lshoulder), 0, sizeof(struct XnVector3D));
  memset(&(this->init_rhand), 0, sizeof(struct XnVector3D));
  memset(&(this->init_lhand), 0, sizeof(struct XnVector3D));

  this->center = 0;
  this->equ_right = 0;
  this->equ_left = 0;
  this->pm_right = 0;
  this->pm_left = 0;
  this->x1 = 0; this->y1 = 0;
  this->x2 = 0; this->y2 = 0;
}

UserControl::UserControl(XnUserID uid, XnUInt32 samples){
  this->uid = uid;
  
  this->cal_state = INIT_STANCE;
  this->num_samples = samples;
  this->samples_needed = samples;

  memset(&(this->init_torso), 0, sizeof(struct XnVector3D));
  memset(&(this->init_rshoulder), 0, sizeof(struct XnVector3D));
  memset(&(this->init_lshoulder), 0, sizeof(struct XnVector3D));
  memset(&(this->init_rhand), 0, sizeof(struct XnVector3D));
  memset(&(this->init_lhand), 0, sizeof(struct XnVector3D));
  
  this->center = 0;
  this->equ_right = 0;
  this->equ_left = 0;
  this->pm_right = 0;
  this->pm_left = 0;
  this->x1 = 0; this->y1 = 0;
  this->x2 = 0; this->y2 = 0;
}

UserControl::~UserControl(){}

UserControl::UserControl(const UserControl &other){
  *this = other;
}

UserControl& UserControl::operator=(const UserControl &other){
  uid = other.uid;
  cal_state = other.cal_state;
  samples_needed = other.samples_needed;
  num_samples = other.num_samples;

  init_torso = other.init_torso;
  init_rshoulder = other.init_rshoulder;
  init_lshoulder = other.init_lshoulder;
  init_rhand = other.init_rhand;
  init_lhand = other.init_lhand;

  center = other.center;
  equ_right = other.equ_right;
  equ_left = other.equ_left;
  pm_right = other.pm_right;
  pm_left = other.pm_left;

  x1 = other.x1;
  y1 = other.y1;
  x2 = other.x2;
  y2 = other.y2;
  return *this;
}


/******************************************************************************
* calibration functions
******************************************************************************/

XnBool UserControl::calibrateStance(
  XnVector3D torso_v,
  XnVector3D rshoulder_v,
  XnVector3D lshoulder_v,
  XnVector3D rhand_v,
  XnVector3D lhand_v
  ){
  
  ipaddvv(&init_torso, &torso_v);
  ipaddvv(&init_rshoulder, &rshoulder_v);
  ipaddvv(&init_lshoulder, &lshoulder_v);
  ipaddvv(&init_rhand, &rhand_v);
  ipaddvv(&init_lhand, &lhand_v);

  return (--samples_needed == 0);
}

XnBool UserControl::calibrateTarget(XnFloat x, XnFloat y){
  if( cal_state == FIRST_TARGET ){
    x1 += x;
    y1 += y;
  }
  else if( cal_state == SECOND_TARGET ){
    x2 += x;
    y2 += y;
  }

  return (--samples_needed == 0);
}


XnBool UserControl::finalizeStance(){
  if( samples_needed != 0 )
    return false;
  else{
    //average of stance calibration
    ipdivvs(&init_torso, num_samples);
    ipdivvs(&init_rshoulder, num_samples);
    ipdivvs(&init_lshoulder, num_samples);
    ipdivvs(&init_rhand, num_samples);
    ipdivvs(&init_lhand, num_samples);

    center = init_torso.X;
    equ_right = init_rshoulder.Y;
    equ_left  = init_lshoulder.Y; 
    pm_right = init_rhand.X;
    pm_left = init_lhand.X;

    return true;
  }
}

XnBool UserControl::finalizeTargets(){
  if( samples_needed != 0 || cal_state != SECOND_TARGET)
    return false;
  else{
    cal_state = USER_CALIBRATED;
    
    //average of target calibration
    x1 /= num_samples;
    y1 /= num_samples;
    x2 /= num_samples;
    y2 /= num_samples;
    
    return true;
  }
}


/******************************************************************************
* mapping function
******************************************************************************/
void UserControl::kinectToOpenGL(XnVector3D *v){
  //check if vector is out of calibration bounds
  if( v->X > x1 ){  v->X = x1; }
  if( v->X < x2 ){  v->X = x2; }
  if( v->Y > y1 ){  v->Y = y1; }
  if( v->Y < y2 ){  v->Y = y2; }

  XnFloat range_x = x1 - x2;
  XnFloat nx1 = x2 - range_x;
  v->X -= 2*(v->X - x2);


  //shift values so they start at 0
  XnFloat shift_x = -nx1;
  XnFloat shift_y = -y2;

  //new upper bound
  XnFloat sx2 = x2 + shift_x; 
  XnFloat sy1 = y1 + shift_y;

  //shift the given vector into the range of all positives
  v->X += shift_x;
  v->Y += shift_y;
  
  //transform the range from all positive to [0, 2]
  v->X /= (sx2 / 2);
  v->Y /= (sy1 / 2);

  //shift the range to [-1, 1];
  v->X -= 1;
  v->Y -= 1;

  //v->X *= -1; //opengl coordinates are flipped
}


/******************************************************************************
* getters
******************************************************************************/

XnUInt32 UserControl::getNeededSamples(){ return samples_needed; }
XnBool UserControl::isCalibrated(){ return cal_state == USER_CALIBRATED; }
XnUserID UserControl::getUserID(){ return uid; }
XnFloat UserControl::getCenter(){ return center; }
XnFloat UserControl::getRightEquator(){ return equ_right; }
XnFloat UserControl::getLeftEquator(){ return equ_left; }
XnFloat UserControl::getRightMeridian(){ return pm_right; }
XnFloat UserControl::getLeftMeridian(){ return pm_left; }
XnFloat UserControl::getInitialZ(){ return init_torso.Z; }

void UserControl::getTopLeft(XnVector3D *retval){
  retval->X = x1;
  retval->Y = y1;
  retval->Z = 0;
}

void UserControl::getBottomRight(XnVector3D *retval){
  retval->X = x2;
  retval->Y = y2;
  retval->Z = 0;
}

user_calibration_state_t UserControl::getCalibrationState(){ return cal_state; }
void UserControl::setCalibrationState(user_calibration_state_t st){
  cal_state = st;
  samples_needed = num_samples;
}


