
#ifndef _USERCONTROL_H
#define _USERCONTROL_H

#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>
#include <XnPropNames.h>
#include <iostream>
#include <string>

using namespace xn;

typedef enum{
  USER_UNCALIBRATED,
  INIT_STANCE,
  FIRST_TARGET,
  SECOND_TARGET,
  USER_CALIBRATED
} user_calibration_state_t;

static std::string userCalibrationStateToString(user_calibration_state_t ucs){
  std::string retval = "";
  switch(ucs){
    case USER_UNCALIBRATED:
      retval = "USER_UNCALIBRATED";
      break;
    case INIT_STANCE:
      retval = "INIT_STANCE";
      break;
    case FIRST_TARGET:
      retval = "FIRST_TARGET";
      break;
    case SECOND_TARGET:
      retval = "SECOND_TARGET";
      break;
    case USER_CALIBRATED:
      retval = "USER_CALIBRATED";
      break;
    default:
      retval = "UNKNOWN";
      break;
  }
  return retval;
}

class UserControl{
  public:
    UserControl(XnUserID);
    UserControl(XnUserID, XnUInt32);
    ~UserControl(void);
    
    UserControl(const UserControl &);
    UserControl & operator=(const UserControl &);

    XnBool calibrateStance(XnVector3D, XnVector3D, XnVector3D,
      XnVector3D, XnVector3D);
    XnBool calibrateTarget(XnFloat, XnFloat);
    XnBool finalizeStance(void);
    XnBool finalizeTargets(void);
    
    


    //setters and getters
    void setCalibrationState(user_calibration_state_t);

    XnUInt32 getNeededSamples(void);
    XnBool isCalibrated(void);
    user_calibration_state_t getCalibrationState(void);
    XnUserID getUserID(void);
    XnFloat getCenter(void);
    XnFloat getRightEquator(void);
    XnFloat getLeftEquator(void);
    XnFloat getRightMeridian(void);
    XnFloat getLeftMeridian(void);
    XnFloat getInitialZ(void);
   
    //get locations of calibrated targets
    void getTopLeft(XnVector3D *);
    void getBottomRight(XnVector3D *);

    //convert kinect coordinates to openGL coordinates (in-place)
    void kinectToOpenGL(XnVector3D *);
    
   friend std::ostream& operator<<(std::ostream&, const UserControl&);
  private:

    XnUserID uid;
    
    //initial position of user joints. used as reference points
    user_calibration_state_t cal_state;
    XnUInt32 samples_needed;
    XnUInt32 num_samples;

    XnVector3D init_torso;  //torso
    XnVector3D init_rshoulder;  //right shoulder
    XnVector3D init_lshoulder;  //left shoulder
    XnVector3D init_rhand;  //right hand
    XnVector3D init_lhand;   //left hand
    

    //reference points used to measure angle of attack of arms
    XnFloat equ_right;  //used to measure the pitch of the right arm
    XnFloat equ_left;   //used to measure the pitch of the left arm

    XnFloat center;   //vertical line that runs through torso and head
    XnFloat pm_right; //measure yaw of right arm
    XnFloat pm_left;  //measure yaw of left arm

    //results of target calibration. these are used to map kinect coordinates
    //to opengl coordinates
    //(x1, y1) -- top left of screen
    //(x2, y2) -- bottom right of screen
    XnFloat x1, y1, x2, y2;
};

inline std::ostream& operator<<(std::ostream &outs, const UserControl &uc){
  std::string s = userCalibrationStateToString(uc.cal_state); 
  outs << "User " << uc.uid << "\n";
  outs << "\tCalibrated: " << s << "\n";
  outs << "\tCenter: " << uc.center << "\n";
  outs << "\tRight Equator: " << uc.equ_right << "\n";
  outs << "\tLeft Equator: " << uc.equ_left << "\n";
  outs << "\tRight Meridian: " << uc.pm_right << "\n";
  outs << "\tLeft Meridian: " << uc.pm_left << "\n\n";
  
  outs << "\tTorso: " << uc.init_torso.X << ", " << uc.init_torso.Y << ", " << uc.init_torso.Z << "\n";
  outs << "\tR.Hand: " << uc.init_rhand.X << ", " << uc.init_rhand.Y << ", " << uc.init_rhand.Z << "\n";
  outs << "\tL.Hand: " << uc.init_lhand.X << ", " << uc.init_lhand.Y << ", " << uc.init_lhand.Z << "\n\n";
  
  outs << "\tTarget 1: " << uc.x1 << ", " << uc.y1 << "\n";
  outs << "\tTarget 2: " << uc.x2 << ", " << uc.y2;
  
  return outs;
}


#endif  // _USERCONTROL_H

