//#include "uicontrol.h"
//#include "sensorcontrol.h"
#include "guicontrol.h"
#include "../unix_utils/utils.h"
#include <stdint.h>
#include <cmath>
#include <cassert>

#define BUF_SIZE 128

// Debugging stuff
typedef enum {
    SWIPEDIR_NONE,
    SWIPEDIR_RIGHT,
    SWIPEDIR_LEFT,
    SWIPEDIR_UP,
    SWIPEDIR_DOWN
} swipedir_t;

#if 0
#include <list>
#include <fstream>
#define TIMER 50
list<glposition_t> data_points;
int i = 0;

void timer(int id) {
    glposition_t loc;

    loc = data_points.front();
    data_points.pop_front();

    message_t * msg = (message_t *)malloc(sizeof(message_t));
    glposition_t * data = (glposition_t *)malloc(sizeof(glposition_t));

    data->x1 = loc.x1;
    data->y1 = loc.y1;

    ++i;
    if (i == 40) {
        data->r = (float)SWIPEDIR_RIGHT;
        msg->type = LH_SWIPE;

        cout << "TIMER" << endl;

        msg->p_data = data;
        addMessage(msg);

        if (!data_points.empty()) {
            glutTimerFunc(TIMER, timer, 0);
        }
    }
    else {
        msg->type = CURSOR_POS;

        msg->p_data = data;

        addMessage(msg);

        if (!data_points.empty()) {
            glutTimerFunc(TIMER, timer, 0);
        }

    }
}

#endif

// Info for previously loaded files
struct file_info {
    GLUI_String file;
    GLUI_String directory;
    GLUI_String file_text;
};

// Callback ids
typedef enum callback_id {
    FILE_BROWSER,
    FILE_TEXTBOX
} callback_id_t;

//screen dimensions
uint32_t W_WIDTH = 1024;
uint32_t W_HEIGHT = 700;

uint32_t cur_width, cur_height;
float cursor_x;
float cursor_y;

//control flags
bool is_quitting;
bool is_fullscreen;
bool kinect_calibrated;
bool do_search;
bool practice_mode;

// Dimensions of the kinect
gldimension_t kinect_dims;

// Live variables passed into GLUI
int                 main_window;
GLUI *              container;
GLUI_FileBrowser *  fb;
GLUI_TextBox *      file_textbox;
GLUI_StaticText *   status;
GLUI_StaticText *   cur_file;

// Unix utils
Utils * utils;

// This is to handle a really weird bug I'm seeing when we auto-navigate the
// filebrowser
bool handle_text_click;

//queue of stuff opengl has to visualize
std::deque<message_t *> inbox;

//all files currently loaded
std::stack<struct file_info> open_file_info;

//"private" function prototypes
void processMessage(void);
void init(void);
void control_cb(int);
bool findDefinition(void);
char * parseDirectory(char * filename);
GLUI_String load_file_text(char * filename);

void do_click_fb();
void do_click_textbox();

// Handle kinect movements
void do_click();
void do_move();
void do_scroll_up();
void do_scroll_down();
void pop_window();

// Map kinect coordinates to screen coordinates
void map_kinect_coords(int x, int y, int * mapped_x, int * mapped_y);


//---- debugging variables below ----
//safe to remove from code
char foo[512];


// OpenGL init
void init(void) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // This sets the OpenGL "real-world" coordinate range
    glOrtho(-1, 1, -1, 1, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Various setup
    is_quitting = false;
    is_fullscreen = false;
    kinect_calibrated = true;
    handle_text_click = true;
    do_search = false;

    cursor_x = 0;
    cursor_y = 0;

/*    
    glposition_t mouse_loc;
    ifstream ins;

    ins.open("trackingdata.csv");
    if (!ins.is_open()) {
        cerr << "Couldn't open tracking file" << endl;
        exit(-1);
    }

    ins >> kinect_dims.x2 >> kinect_dims.y1 >>
        kinect_dims.x1 >> kinect_dims.y2;

    cerr << "Kinect dims (x): " << kinect_dims.x1 << " to " << kinect_dims.x2 << endl;
    cerr << "Kinect dims (y): " << kinect_dims.y2 << " to " << kinect_dims.y1 << endl;
    
    while (ins >> mouse_loc.x1 >> mouse_loc.y1) {
        data_points.push_back(mouse_loc);
    }

    ins.close();

    glutTimerFunc(TIMER, timer, 0);
*/  
}

char * parseDirectory(char * filename) {
    // Walk from the end, looking for the first '/'
    char * directory;
    size_t len = strlen(filename);
    int i;

    if ((directory = (char *)malloc(sizeof(char ) * (len + 1))) == NULL) {
        perror("Failed to malloc");
        exit(-1);
    }

    strncpy(directory, filename, len + 1);

    for (i = len - 1; i >= 0; i--) {
        if (*(directory + i) == '/') {
            *(directory + i + 1) = '\0';
            return directory;
        }
    }

    return NULL;
}

GLUI_String load_file_text(char * filename) {
    GLUI_String text;
    FILE * file;
    char c;

    printf("filename: %s\n", filename);

    if ((file = fopen(filename, "r")) == NULL) {
        perror("Error opening file");
    }
    else {
        while ((c = getc(file)) != EOF) {
            text += c;
        }

        fclose(file);
    }

    return text;
}

bool findDefinition(void) {
    GLUI_String text;
    FILE * file;
    string filename;
    char c;
    char line[BUF_SIZE];
    char DELIM[] = " \t\n\r\f\v";
    char * to_open;
    char * file_found;
    char * directory_found;
    char * token;
    int i;

    list<string> search;
    struct file_info info;

    filename = fb->get_file();
    
    printf("filename: %s\n", filename.c_str());

    if ((file = fopen(filename.c_str(), "r")) == NULL) {
        perror("Error opening file");
        return false;
    }
   
    // Selected line is in file_textbox->curr_line
    for (i = 0; i < file_textbox->curr_line; i++) {
        fgets(line, BUF_SIZE, file);
    }

    fgets(line, BUF_SIZE, file);
    // line is now what we want
    
    // Push line into an stl list
    token = strtok(line, DELIM);
    do {
        search.push_back(token);
    } while ((token = strtok(NULL, DELIM)) != NULL);

    if ((file_found = utils->findDefinition(search)) == NULL) {
        std::cerr << "Could not find definition" << endl;

        fclose(file);
        return false;
    }
    else {
        if ((directory_found = parseDirectory(file_found)) == NULL) {
            info.directory = ".";
        }
        else {
            info.directory = directory_found;
        }

        std::cerr << "Found definition for in " << file_found << 
            " (directory: " << info.directory << ")" << endl;

        // Load text
        text = load_file_text(file_found);

        // Save entry
        info.file = file_found;
        info.file_text = text;
        open_file_info.push(info);

        // Set text
        file_textbox->set_text(text.c_str());
        
        // Set directory
        // NOTE: Must chdir must be done before fbreaddir
        chdir(info.directory.c_str());
        fb->set_file(info.file);
        fb->fbreaddir(info.directory.c_str());

        // Handle the weird bug
        handle_text_click = false;

        fclose(file);
        return true;
    }
}

/******************************************************************************
* general control
******************************************************************************/
// Control callback
void control_cb(int control) {
    
    switch (control) {
        case FILE_BROWSER:
            do_click_fb();
            break;

        case FILE_TEXTBOX:
            if (!handle_text_click) {
                handle_text_click = true;
                break;
            }

            do_click_textbox();
            break;

        default:
            break;
    }

    glutPostRedisplay();
}

void do_click_fb() {
    // Clicked on a file in the browser - so, top of stack is our entry
    GLUI_String text;
    string filename;
    struct file_info info;

    filename = fb->get_file();
    //cout << "Clicked on file: " << filename.c_str() << endl; 
    text = load_file_text((char *)filename.c_str());

    // Store the info correctly
    info = open_file_info.top();
    info.file_text = text;

    cur_file->set_text(filename.c_str());

    file_textbox->set_text(text.c_str());
}

void do_click_textbox() {
    // Clicked on a line of text in the textbox
    // Set the status, do_search <-- true, queue re-display
    status->set_text("Searching...");
    do_search = true;
}

// Perform a selection
void do_click() {
    // Hacked glui to let us do this
    /*glutWarpPointer(cursor_x, W_HEIGHT - cursor_y);
    container->do_mouse_click(GLUT_LEFT, GLUT_DOWN, cursor_x, W_HEIGHT - cursor_y);
    container->do_mouse_click(GLUT_LEFT, GLUT_UP, cursor_x, W_HEIGHT - cursor_y);*/

    fb->mouse_down_handler(0, -1);
}

// Move the mouse cursor
void do_move() {
    // I didn't realize this existed. wow
    //cout << "do move: " << cursor_x << ", " << cursor_y << endl;
    glutWarpPointer(cursor_x, W_HEIGHT - cursor_y);
    container->do_mouse_motion(cursor_x, W_HEIGHT - cursor_y);
}

// "Scroll up" on the open window
void do_scroll_up() {
    // big time hack
    // x == -1 -> scroll up
    file_textbox->scrollbar->mouse_down_handler(-1, 0);
    file_textbox->scrollbar->mouse_up_handler(-1, -1, 0);
    file_textbox->scrollbar->redraw();
}

// "Scroll down" on the open window
void do_scroll_down() {
    // big time hack
    // x != -1 && y == -1 -> scroll down
    file_textbox->scrollbar->mouse_down_handler(0, -1);
    file_textbox->scrollbar->mouse_up_handler(-1, -1, 0);
    file_textbox->scrollbar->redraw();
}

// Perform a file close
void pop_window(void) {
    struct file_info info;

    if (open_file_info.size() == 1) {
        cout << "No previous windows...\n";
        return;
    }

    open_file_info.pop();

    info = open_file_info.top();
    
    cout << "Dir: " << info.directory << endl;
    cout << "File: " << info.file << endl;
    
    // Set directory
    // NOTE: Must chdir must be done before fbreaddir
    chdir(info.directory.c_str());
    fb->fbreaddir(info.directory.c_str());

    if (info.file.size() > 0)
        fb->set_file(info.file);

    // Set text
    file_textbox->set_text(info.file_text.c_str());
}

void map_kinect_point(float  x_loc, float y_loc, float * mapped_x, float * mapped_y) {
    float x_map;
    float y_map;
    


    //cerr << "----------------------------\n";
    //cerr << "Mapping " << x_loc << ", " << y_loc << endl; 
    
    if (!kinect_calibrated) {
        *mapped_x = x_loc;
        *mapped_y = y_loc;
        return;
    }

    // Flip x over y-axis
    //float u = (kinect_dims.x1 + kinect_dims.x2) / 2;
    //x_loc = u - (x_loc - u);


//    cout << x_loc << ", " << y_loc << endl;

    x_map = (float)(x_loc - kinect_dims.x1) /
            (float)(kinect_dims.x2 - kinect_dims.x1);

    y_map = (float)(y_loc - kinect_dims.y2) /
            (float)(kinect_dims.y1 - kinect_dims.y2);

    if ((x_loc < kinect_dims.x1 || x_loc > kinect_dims.x2) ||
        (y_loc < kinect_dims.y2 || y_loc > kinect_dims.y1)) {
        // Invalid point - out of the kinect dimensions
        *mapped_x = 0;
        *mapped_y = 0;
        /*
        printf("error: %.2f, %.2f ( %.2f, %.2f, %.2f, %.2f )\n", x_loc, y_loc, 
          kinect_dims.x1,
          kinect_dims.x2,
          kinect_dims.y1,
          kinect_dims.y2
        );
        */
        return;
    }

    if (!(x_map > 0 && y_map > 0 && x_map < 1 && y_map < 1)) {
        // Something went wrong
        cerr << "INVALID MAPPING" << endl;
        cerr << "x_loc: " << x_loc << ", y_loc: " << y_loc << endl;
        cerr << "x_map: " << x_map << ", y_map: " << y_map << endl;
        assert(1 == 0);
    }

    *mapped_x = x_map * W_WIDTH;
    *mapped_y = y_map * W_HEIGHT;
    
    //cerr << "Mapped to: " << *mapped_x << ", " << *mapped_y << endl;

}

// GLUT/GLUI control
void initGuiControl(int *pargc, char **pargv){
  char * root;
  char rootdir[BUF_SIZE];
  struct file_info info;

  if (*pargc != 5) {
    std::cerr << "Usage: " << *pargv << " <width> <height> <practice_mode> <root_dir>" << endl;
    exit(-1);
  }

  W_WIDTH = atoi(*(pargv + 1));
  W_HEIGHT = atoi(*(pargv + 2));
  practice_mode = ( atoi(*(pargv + 3)) > 0 ) ? true : false;
  root = *(pargv + 4);
  
  glutInit(pargc, pargv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(W_WIDTH, W_HEIGHT);
  glutInitWindowPosition(0, 0);
  main_window = glutCreateWindow("CS2610: Final Project");

  // Some OpenGL initialization
  init();

  // Register callbacks with GLUI, not GLUT
  GLUI_Master.set_glutKeyboardFunc(glKeyPress);
  GLUI_Master.set_glutDisplayFunc(glUpdateFrame);
  GLUI_Master.set_glutIdleFunc(glIdleFrame);
  GLUI_Master.auto_set_viewport();


  // chdir before creating filebrowser
  chdir(root);
  getcwd(rootdir, BUF_SIZE);
  printf("rootdir: %s\n", rootdir);
  
  // Save file info
  info.directory = rootdir;
  info.file_text = "File contents displayed here";
  open_file_info.push(info);

  // Finally, create GLUI stuff
  container = GLUI_Master.create_glui_subwindow(main_window, GLUI_SUBWINDOW_LEFT);
  container->set_main_gfx_window(main_window);
  container->hide();

  GLUI_Panel * ep = new GLUI_Panel(container, "", true);

  fb = new GLUI_FileBrowser(ep, "File Browser", GLUI_PANEL_EMBOSSED, FILE_BROWSER, control_cb);
  fb->set_h(W_HEIGHT * .85);
  fb->set_w(W_WIDTH * .2);

  cur_file = new GLUI_StaticText(ep, "");
  new GLUI_Column(ep, false);

  file_textbox = new GLUI_TextBox(ep, true, FILE_TEXTBOX, control_cb);
  file_textbox->set_text("File contents displayed here");
  file_textbox->set_h(W_HEIGHT * .85);
  file_textbox->set_w(W_WIDTH * .7);
  file_textbox->enable();
  file_textbox->set_font(GLUT_BITMAP_HELVETICA_12);

  status = new GLUI_StaticText(ep, "Ready");

  // Hide it at first
  utils = new Utils();
  utils->setSourceRootDir(root);
  //initSensorControl(pargc, pargv); //this needs a pointer to addMessage
  //sensorControlMessageHandler(addMessage);
  //
}

void startGuiControl(){
  //startSensorControl();
  glutMainLoop();
}


/******************************************************************************
* openGL callbacks
******************************************************************************/
void glUpdateFrame(){
  if( is_quitting ){
    glClose();
  }
  else{
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(1.0f, 1.0f, 1.0f, 0);
    
    //resize screen to match area covered by kinect field of view
    if( kinect_calibrated && !practice_mode){
      container->show();
      
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();

      glOrtho(kinect_dims.x2, kinect_dims.x1, kinect_dims.y1, kinect_dims.y2, -1.0, 1.0);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
    }

    //resize window
    if( is_fullscreen ){
      glutFullScreen();
    }
    else{
      glutReshapeWindow(W_WIDTH, W_HEIGHT);
    }
    
    cur_width = glutGet(GLUT_WINDOW_WIDTH);
    cur_height = glutGet(GLUT_WINDOW_HEIGHT);

    //update all control components
    //updateSensor();

    //process any messages
    processMessage();
    //---- temp stuff, feel free to delete ----/
    /*
    glColor3f(0, 0, 0);
    float tx = .5, ty = .5;
    glUnnormalizeXY(&tx, &ty);
    glString(tx, ty, foo);
    */
    //---- end temp stuff ----/

    // Move cursor
    // glutWarpPointer(render_x, W_HEIGHT - render_y);
    
    

    glutSwapBuffers();
  }
}

void glIdleFrame(){
  /* According to GLUT specification, the current window is undefined
   * during and idle callback. So we need to explicitly change it if necessary
   */
  if( is_quitting )
    glClose();
  else if (glutGetWindow() != main_window) {
    if (do_search) {
      if (findDefinition()) {
        status->set_text("Definition Found!");
        cur_file->set_text(open_file_info.top().file.c_str());
      }
      else {
        status->set_text("Could not find definition");
      }

      do_search = false;
    }

    glutSetWindow(main_window);
    glutPostRedisplay();
  }
}

void glClose(){
  glutDestroyWindow(main_window);

  //end all control components
  //endSensorControl();
}

void glKeyPress(unsigned char key, int x, int y){
  switch(key){
    case 27:  //esc
      is_quitting = true;
      break;
    case 'f':
      is_fullscreen = !is_fullscreen;
      break;
    default:
      std::cout << "Key Pressed!\n";
      break;
  }
}

/******************************************************************************
* mesage processing
******************************************************************************/
void addMessage(message_t *msg){
  if( msg )
    inbox.push_back(msg);
}

void processMessage(){
  message_t *msg;
  glshape_data_t sdata;
  gltext_data_t tdata;
  gldimension_t ddata;
  glposition_t ptdata;

  float mapped_x1, mapped_x2;
  float mapped_y1, mapped_y2;

  // Do not process messages during a search
  if (do_search) {
      inbox.clear();
      return;
  }

  while( !inbox.empty() ){
    mapped_x1 = 0;
    mapped_x2 = 0;
    mapped_y1 = 0;
    mapped_y2 = 0;
  
    msg = inbox.front();
    switch(msg->type){
      case DRAW_RECT:
        sdata = *(glshape_data_t *)(msg->p_data);
        map_kinect_point(sdata.x1, sdata.y1, &mapped_x1, &mapped_y1);
        map_kinect_point(sdata.x2, sdata.y2, &mapped_x2, &mapped_y2);

        glColor3f(sdata.r, sdata.g, sdata.b);
        glRectangle(mapped_x1, mapped_y1, mapped_x2, mapped_y2);
        break;
      case DRAW_ELLIPSE:
        sdata = *(glshape_data_t *)(msg->p_data);
        map_kinect_point(sdata.x1, sdata.y1, &mapped_x1, &mapped_y1);
        map_kinect_point(sdata.x2, sdata.y2, &mapped_x2, &mapped_y2);

        glColor3f(sdata.r, sdata.g, sdata.b);
        glCircle(mapped_x1, mapped_y1, mapped_x2);
        break;
      case DRAW_TEXT:
        tdata = *(gltext_data_t *)(msg->p_data);
        
        //draw text uses normalized coordinate values [0-1] instead of absolute
        //values.
        glUnnormalizeXY(&tdata.x1, &tdata.y1);
        glColor3f(tdata.r, tdata.g, tdata.b);
        glString(tdata.x1, tdata.y1, tdata.s);
        
        break;
      case SET_DIM:
        ddata = *(gldimension_t *)(msg->p_data);
        kinect_calibrated = true;
        kinect_dims.x1 = ddata.x2; kinect_dims.y2 = ddata.y2; //min values
        kinect_dims.x2 = ddata.x1; kinect_dims.y1 = ddata.y1;  //max values

        // Set the new bounding box here
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0, W_WIDTH, 0.0, W_HEIGHT, -1.0, 1.0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        break;
      case CURSOR_POS:{
        // Move the cursor
        ptdata = *(glposition_t *)(msg->p_data);
        //cout << ptdata.x1 << ", " << ptdata.y1 << endl;
        map_kinect_point(ptdata.x1, ptdata.y1, &mapped_x1, &mapped_y1);
        
        glColor3f(ptdata.r, ptdata.g, ptdata.b);
        glCircle(mapped_x1, mapped_y1, .01 * W_WIDTH);

        if (mapped_x1 != 0) {
            cursor_x = mapped_x1;
        }
        if (mapped_y1 != 0) {
            cursor_y = mapped_y1;
        }
        
        do_move();
        }
        break;
      case CURSOR_SEL:{
        cerr << "NOT HANDLING CURSOR_SEL\n\n\n";
        // Simulate a click of the textbox at the cursor
        /*
        ptdata = *(glposition_t *)(msg->p_data);
        map_kinect_point(ptdata.x1, ptdata.y1, &mapped_x1, &mapped_y1);
        
        if (mapped_x1 != 0) {
            cursor_x = mapped_x1;
        }
        if (mapped_y1 != 0) {
            cursor_y = mapped_y1;
        }

        printf("Clicked: %.2f, %.2f\n", cursor_x, cursor_y);
        do_click();*/
        }
        break;
        
      case LH_SWIPE:{
        ptdata = *(glposition_t *)(msg->p_data);
        
        //location associated with the swipe
        //ptdata.x1, ptdata.y1
        map_kinect_point(ptdata.x1, ptdata.y1, &mapped_x1, &mapped_y1);
        
        //swipe direction: SWIPEDIR_LEFT, SWIPEDIR_RIGHT,
        //SWIPEDIR_UP, SWIPEDIR_DOWN
        swipedir_t dir = (swipedir_t)ptdata.r;
        
        status->set_text(ptdata.s);
        cout << ptdata.s << endl;

        switch (dir) {
          case SWIPEDIR_RIGHT:
            // Open
            // Simulate a click of the textbox at the cursor
            if (mapped_x1 != 0) {
                cursor_x = mapped_x1;
            }
            if (mapped_y1 != 0) {
                cursor_y = mapped_y1;
            }

            printf("Clicked: %.2f, %.2f\n", cursor_x, cursor_y);
            do_click();
            break;
          case SWIPEDIR_LEFT:
            // Close
            pop_window();
            break;
          case SWIPEDIR_UP:
            // Scroll up
            do_scroll_up();
            break;
          case SWIPEDIR_DOWN:
            // Scroll down
            do_scroll_down();
            break;
          case SWIPEDIR_NONE:
            break;
        }

        }
        break;
      default:
        break;
    }
    
    inbox.pop_front();

    //we're done with this message
    //free(msg);
  }
}

/******************************************************************************
* custom drawing functions
******************************************************************************/
void glCircle(float x1, float y1, float r){
  glBegin(GL_TRIANGLE_FAN);
  glVertex2f(x1,y1);

  float x2, y2;
  for(float angle = 0.0f; angle < 360.0f; angle+=.2f){
    x2 = x1+sin(angle)*r;
    y2 = y1+(cos(angle)*r);
    glVertex2f(x2,y2);
  }
  glEnd();
}

//these coordinates passed in as arguments must be normalized so their
//range is between -1 and 1 inclusive
void glRectangle(float nx1, float ny1, float nx2, float ny2){
  float width, ux; 
  float height, uy; 
  if( kinect_calibrated ){
    width = abs(kinect_dims.x1 - kinect_dims.x2);
    height = abs(kinect_dims.y1 - kinect_dims.y2);

    ux = (kinect_dims.x1 + kinect_dims.x2) / 2;
    uy = (kinect_dims.y1 + kinect_dims.y2) / 2;

    //convert normalized values to the range of the screen
    nx1 = ux + (nx1 * (width / 2));
    nx2 = ux + (nx2 * (width / 2));
    ny1 = uy + (ny1 * (height / 2));
    ny2 = uy + (ny2 * (height / 2));
  }
  else{
    width = cur_width;
    height = cur_height;
  }

  float ratio = (1.0*width / height);
  
  float w = nx2 - nx1;
  float h = (ny2 - ny1) / ratio;

  glRectf(nx1, ny1, nx2, ny2);
}

//requires absolute position values x and y
void glString(float x, float y, char *string) {
  char *c;
  glRasterPos2f(x, y);
  for (c=string; *c != '\0'; c++) 
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
}

//converts coordinate values ranging from [0 - 1] into their respective
//values in the current view
void glUnnormalizeXY(float *unx, float *uny){
  if( kinect_calibrated ){
    *unx = *unx * W_WIDTH;
    *uny = *uny * W_HEIGHT;
  }
  else{
    *unx = (*unx * 2) - 1;
    *uny = (*uny * 2) - 1;
  }
}
