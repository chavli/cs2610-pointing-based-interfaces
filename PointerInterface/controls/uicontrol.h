#ifndef _UICONTROL_H
#define _UICONTROL_H

#include "../comm/message.h"
#include <GL/glui.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <deque>
#include <string>
#include <iostream>
#include <stack>

using namespace std;

//opengl control
void initGuiControl(int *, char **);
void startGuiControl(void);
void pauseGuiControl(void);
void endGuiControl(void);

//opengl callbacks
void glKeyPress(unsigned char, int, int);
void glUpdateFrame(void);
void glIdleFrame(void);
void glClose(void);

//custom drawing functions
void glCircle(float, float, float);
void glRectangle(float, float, float, float);
void glString(float, float, char *);
void glNormalizeXY(float *, float *);
void glUnnormalizeXY(float *, float *);

//add work to queue
void addMessage(message_t *);


#endif  // _UICONTROL_H





