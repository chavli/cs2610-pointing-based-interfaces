
#ifndef _POINTERCONTROL_H
#define _POINTERCONTROL_H

//checks that the return code reports a successful execution 
//and if not prints an error message and stops the application execution
#define CHECK_RC(rc, what)\
  if (rc != XN_STATUS_OK){\
    printf("%s failed: %s\n", what, xnGetStatusString(rc));\
  }

#include "../comm/message.h"

#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>
#include <XnPropNames.h>
#include <set>
#include <iostream>

typedef enum{
  TG_NONE,
  TG_TOP_LEFT,
  TG_BOTTOM_RIGHT,
  TG_CENTER,
} target_t;

typedef enum{
  SWIPEDIR_NONE,
  SWIPEDIR_RIGHT,
  SWIPEDIR_LEFT,
  SWIPEDIR_UP,
  SWIPEDIR_DOWN
} swipedir_t;

using namespace xn;

//public control functions
void initSensorControl(int *, char **);
void startSensorControl(void);
void endSensorControl(void);
void sensorControlMessageHandler( void (*)(message_t *) );

void updateSensor(void);

#endif // _POINTERCONTROL_H

