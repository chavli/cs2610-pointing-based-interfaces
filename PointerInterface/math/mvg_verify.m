function [p] = mvg_verify(x, mu, sigma)
	x  = x - mu;
	p = (1 / ((2 * pi)^(1.5)  * det(sigma)^(.5))) * exp(-.5 * (x * pinv(sigma) * x') );
	

