/*
*
* keeps track of and models a bivariate gaussian distribution
*
*
*/


#ifndef _GAUSSIANSET_H
#define _GAUSSIANSET_H

#define DEBUG

#include <armadillo> //cpp linear algebra library
#include <cmath>
#include <stdint.h>

using namespace arma;

class GaussianSet{

  public:
    
    GaussianSet(uint32_t);
    GaussianSet(uint32_t, uint32_t, uint32_t, float, float);
    ~GaussianSet();
    
    bool add(fmat);
    float similarity(fmat, bool);
    void reset(void);

    bool isFull(void);
    bool isParameterized(void);

    bool isSimilar(fmat);
    bool isOutlier(fmat);

    #ifdef DEBUG
      fmat data;
      fmat cm; 
      fmat mu;
      void updateParams(void);
    #endif
  private:
    uint32_t capacity;
    uint32_t update_freq;
    uint32_t update_ct; 
    uint32_t dim;       //dimensionality
    
    //normalization constant
    float alpha;
    bool parameterized;
    
    float s_thresh; //sample similarity > s_thresh is considered similar
    float o_thresh; //sample similarity < o_thresh is considered an outlier
    
    #ifndef DEBUG
      //parameters defining the multivariate gaussian
      fmat data;  //all data
      fmat cm;    //covariance matrix
      fmat mu;    //means
      void updateParams(void);
    #endif
};


#endif  // _TIMESERIES_H



