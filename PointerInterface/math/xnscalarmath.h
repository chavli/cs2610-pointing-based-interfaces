
#ifndef _SCALARMATH_H
#define _SCALARMATH_H

#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <XnPropNames.h>
#include <cmath>

using namespace xn;

static const XnFloat PI = 3.141592653589793238;

static XnFloat degToRad(XnFloat degs){
  return degs * (PI / 180);
}

static XnFloat radToDeg(XnFloat rads){
  return rads * (180 / PI);
}



#endif  // _SCALARMATH_H


