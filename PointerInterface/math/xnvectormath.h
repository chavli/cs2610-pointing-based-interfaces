
#ifndef _VECTORMATH_H
#define _VECTORMATH_H

#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <XnPropNames.h>
#include <cmath>

using namespace xn;

//in-place vector-vector add
static void ipaddvv(XnVector3D *dest, XnVector3D *other){
  dest->X += other->X;
  dest->Y += other->Y;
  dest->Z += other->Z;
}

//in-place vector-scalar divide
static void ipdivvs(XnVector3D *dest, XnFloat s){
  dest->X /= s;
  dest->Y /= s;
  dest->Z /= s;
}

//in-place element-wise vector-vector multiplication
static void ipmulvv(XnVector3D *dest, XnVector3D *other){
  dest->X *= other->X;
  dest->Y *= other->Y;
  dest->Z *= other->Z;
}

//extrapolates the delta of a vector given pitch, yaw, and distance
static XnVector3D* extendv(XnFloat pitch, XnFloat yaw, XnFloat distance){
  XnVector3D *delta = (XnVector3D *)malloc(sizeof( struct XnVector3D ));
  delta->X = distance * tan(yaw);
  delta->Y = distance * tan(pitch);

  return delta;
}

static XnVector3D* addvv(XnVector3D *v1, XnVector3D *v2){
  XnVector3D *v3 = (XnVector3D *)malloc(sizeof( struct XnVector3D ));
  v3->X = v1->X + v2->X;
  v3->Y = v1->Y + v2->Y;
  v3->Z = v1->Z + v2->Z;
  
  return v3;
}

static XnVector3D* subvv(XnVector3D *v1, XnVector3D *v2){
  XnVector3D *v3 = (XnVector3D *)malloc(sizeof( struct XnVector3D ));
  v3->X = v1->X - v2->X;
  v3->Y = v1->Y - v2->Y;
  v3->Z = v1->Z - v2->Z;

  return v3;
}

static XnFloat magv(XnVector3D *v1){
  return sqrt( pow(v1->X, 2) + pow(v1->Y, 2) + pow(v1->Z, 2));
}

static XnFloat distvv(XnVector3D *v1, XnVector3D *v2){
  XnFloat m = 0;
  //convert both to unit vectors
  m = magv(v1);
  ipdivvs(v1, m);
  
  m = magv(v2);
  ipdivvs(v2, m);

  XnVector3D *v3 = subvv(v1, v2);
  m = magv(v3);
  free(v3);

  return m;
}

#endif  //_VECTORMATH_H

