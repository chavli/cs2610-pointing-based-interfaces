#include "gaussianset.h"
#include "xnvectormath.h"
#include <XnOpenNI.h>                                                               
#include <XnCodecIDs.h>     
#include <XnCppWrapper.h>     
#include <XnPropNames.h>   
#include <iostream>

using namespace std;
using namespace arma;

int main(int argc, char **argv){
  XnVector3D v1 = {3, 0, 0};
  XnVector3D v2 = {-4, 0, 0};
  
  float m = distvv(&v1, &v2);
  cout << m << endl;
  /*
  GaussianSet gs(3, 20, 20, 0, 0);
  
  fmat test_data = zeros<fmat>(0, 3); 
  
  int n = 10;
  float x[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  float y[] = {9, 2, 6, 4, 1, 7, 2, 1, 4, 2};
  float z[] = {1, 7, 6, 1, 7, 3, 0, 3, 1, 9};
  

  for(int i = 0; i < n; ++i){
    fmat row = zeros<fmat>(0, 3);
    row << x[i] << y[i] << z[i] << endr;
    test_data = join_cols(test_data, row);
  }
  
  test_data.print("test_data = ");
  

  gs.add(test_data.rows(0, 0));
  gs.data.print("gs.data = ");

  gs.add(test_data);
  gs.add(test_data.rows(0, 4));

  gs.data.print("gs.data = ");
  gs.cm.print("gs.cm = ");
  gs.mu.print("gs.mu = ");
  
  gs.add(test_data.rows(0, 4));
  gs.data.print("gs.data = ");
  gs.cm.print("gs.cm = ");
  gs.mu.print("gs.mu = ");

  fmat row;
  row << x[0] << y[0] << z[0] << endr;
  float p = gs.similarity(gs.mu, false);
  cout << "similarity: " << p << "\n";
  
  cout << "-------------------" << endl;

  gs.add(test_data.rows(5, 9));
  gs.data.print("gs.data = ");
  gs.cm.print("gs.cm = ");
  gs.mu.print("gs.mu = ");
  */

  return 0;
    
}



