
#include "gaussianset.h"

#define ISNAN(x) (x != x)

using namespace arma;

GaussianSet::GaussianSet(uint32_t D){
  this->capacity = 100;
  this->update_freq = 50;
  this->update_ct = 0;
  this->alpha = 0;
  this->dim = D;

  this->parameterized = false;
  this->s_thresh = .68f;
  this->o_thresh = .2f;

  this->data = zeros<fmat>(0, D);
  this->cm = zeros<fmat>(D, D);
  this->mu = zeros<fmat>(0, D);
}

GaussianSet::GaussianSet( uint32_t D, 
                          uint32_t capacity,
                          uint32_t update,
                          float sim_thresh,
                          float out_thresh){

  this->capacity = capacity;
  this->update_freq = update;
  this->update_ct = 0;
  this->alpha = 0;
  this->dim = D;
  
  this->parameterized = false;
  this->s_thresh = sim_thresh;
  this->o_thresh = out_thresh;

  this->data = zeros<fmat>(0, D);
  this->cm = zeros<fmat>(D, D);
  this->mu = zeros<fmat>(0, D);
}

GaussianSet::~GaussianSet(){
}

bool GaussianSet::add(fmat samples){
    
  //check dimensionality first
  if( samples.n_cols != dim )
    return false;
  
  //make space for new data if needed
  if( (capacity - data.n_rows) < samples.n_rows ){
    float cut = samples.n_rows - (capacity - data.n_rows);
    data = data.rows(cut, data.n_rows-1);
  }
  
  //add new data
  data = join_cols(data, samples);
  
  //update parameters
  update_ct += samples.n_rows;
  if( update_ct >= update_freq ){
    update_ct = 0;
    updateParams();
  }
  return true;
}

float GaussianSet::similarity(fmat sample, bool normalized){
  float p = 0;

  //dimensionality must be the same and there must be 
  //exactly one sample
  if(sample.n_cols != dim || sample.n_rows != 1)
    return 0;
  
  //calculate multivariate distribution if the model has been
  //parameterized
  if( parameterized ){
    sample = sample - mu;
  
    fmat icm = pinv(cm);
  
    p = 1 / ( pow(2*3.1415926535, dim/2.0) * sqrt(det(cm)));
    
    fmat temp = sample * icm * trans(sample);
    float s = temp(0);
    p = p *exp(-.5f * s );
   
    if( normalized )
      p /= alpha;
  }
  
  if( ISNAN(p) )
    return 0;

  return p;
}

void GaussianSet::reset(){
  data = zeros<fmat>(0, dim);
  cm = zeros<fmat>(dim, dim);
  mu = zeros<fmat>(0, dim);
  update_ct = 0;
  alpha = 0;
  parameterized = false;
}

bool GaussianSet::isFull(){ return data.n_rows == capacity; }

bool GaussianSet::isParameterized(){ return parameterized; }

bool GaussianSet::isSimilar(fmat sample){
  return (similarity(sample, true) > s_thresh) || !parameterized;
}

bool GaussianSet::isOutlier(fmat sample){
  return (similarity(sample, true) < o_thresh) || !parameterized;
}

void GaussianSet::updateParams(){
  mu = mean(data);
  cm = cov(data);
  
  alpha = similarity(mu, false);
  parameterized = true;
}

