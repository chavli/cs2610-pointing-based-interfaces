// Just testing the utilities

#include "utils.h"
#include <iostream>
#include <list>
#include <string>

using namespace std;

//#define ROOT_SRC "/afs/cs.pitt.edu/usr0/briankoco/private/courses/fall2012/cs2610/projects/cs2610_final/PointerInterface/"
#define ROOT_SRC ".."

int main(int agrc, char ** argv) {
    Utils u;
    char * filename;
    FILE * fp;
    char buf[128];

    u.setSourceRootDir((char *)ROOT_SRC);

    list<string> items;
    items.push_back("blah");
    items.push_back("string");
    items.push_back("XnStack");
    items.push_back("1235");

    if ((filename = u.findDefinition(items)) == NULL) {
        cout << "Couldn't find it" << endl;
    }
    else {
        cout << "Found it. filename = " << filename << endl;

        if ((fp = fopen(filename, "r")) == NULL) {
            perror("Could not open file");
            return -1;
        }
        else {
            while ((fgets(buf, 128, fp))) {
                fputs(buf, stdout);
            }
            fputs("\n", stdout);
        }
        free(filename);
    }
}
