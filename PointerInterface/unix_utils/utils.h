#ifndef _UTILS_H
#define _UTILS_H

#include <sys/types.h>
#include <sys/dir.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <unistd.h>

#include <list>
#include <string>
#include <iostream>
using namespace std;

#define SOURCE_EXT ".cpp .c .cc .h"

// These class defines unix utilities to be used by the backend parser

/* Currently, we can find types as long as both of the following condition is
 * met:
 * (1) The passed in substring is of the form:
 *      'struct' <type> are the first two words in the string
 *      'class' <type> are the first two words in the string
 *      <type> is the first word in the string
 *
 * (2) The type itself is of type "struct" or "class"
 *
 *  For most source trees this should not be a huge problem
 */
typedef enum data_type {
    STRUCT,
    CLASS,
    ENUM
} data_type_t;

class Utils {
    public:
        Utils();
        ~Utils();

        // Root directory of the project being parsed
        void setSourceRootDir(char * dir);

        // Find file that is housing the definition of type in substr
        // NOTE: the value returned is allocated off the heap and must be free'd
        // by the caller
        char * findDefinition(list<string> substr);

    private:
        // Root of the source directory
        char * root;

        // Private helpers
        char * findFile(char * type);
        char * findFile(char * type, char * directory);
        bool hasTypeDefinition(data_type_t data_type, char * type, char * filename);

};

#endif /* _UTILS_H */
