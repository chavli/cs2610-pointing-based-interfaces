#include "utils.h"
#include <cassert>

#define BUF_SIZE 128

Utils::Utils() {
    root = NULL;
}

Utils::~Utils() {
    if (root)
        free(root);

    root = NULL;
}

void Utils::setSourceRootDir(char * dir) {
    char rootdir[BUF_SIZE];

    chdir(dir);
    getcwd(rootdir, BUF_SIZE);

    root = (char *)malloc(BUF_SIZE);
    memset(root, 0, BUF_SIZE);

    strncpy(root, rootdir, BUF_SIZE);
}

char * Utils::findDefinition(list<string> type) {
    char * result;

    for (list<string>::iterator it = type.begin(); it != type.end(); it++) {
        cerr << "token: " << *it << endl;
        
        if ((result = findFile((char *)it->c_str(), root)) != NULL) {
            return result;
        }
    }

    return NULL;
}

// Private
int dir_filter(const struct dirent * dir) {
    char * ext;
    char buf[BUF_SIZE];

    // Directory
    if (dir->d_type == DT_DIR) {
        if (!strncmp(".", dir->d_name, 1)) {
            return 0;
        }
        else {
            return 1;
        }
    }
    else {
        strncpy(buf, (char *)SOURCE_EXT, strlen((char *)SOURCE_EXT));

        ext = strtok(buf, " ");
        for (; ext != NULL; ext = strtok(NULL, " ")) {
            if (strstr(dir->d_name, ext)) {
                return 1;
            }
        }

        return 0;
    }
}

int dir_cmp(const dirent  ** d1, const dirent ** d2) {
    return (*d1)->d_name - (*d2)->d_name;
}

char * Utils::findFile(char * type, char * directory) {
    struct dirent ** namelist = NULL;
    struct dirent * current = NULL;
    char * search = NULL;
    char * filename;

    size_t dir_len = 0;
    size_t next_file_len = 0;
    size_t search_len = 0;

    int dir_entries = scandir(directory, &namelist, dir_filter, dir_cmp); 

    //printf("Scanning %s\n", directory);

    for (int i = 0; i < dir_entries; ++i) {
        current = namelist[i];

        dir_len = strlen(directory);
        next_file_len = strlen(namelist[i]->d_name);
        search_len = dir_len + 1 + next_file_len + 1;

        if ((search = (char *)realloc(search, sizeof(char) * search_len)) == NULL) {
            perror("Failed to malloc");
            return NULL;
        }
        memset(search, 0, search_len);

        // Create the fully qualified path
        strncpy(search, directory, dir_len);
        strncat(search, "/", 1);
        strncat(search, namelist[i]->d_name, next_file_len);
        strncat(search, "\0", 1);

        if (current->d_type == DT_DIR) {
            if ((filename = findFile(type, search)) != NULL) {
                free(search);
                return filename;
            }
        }
        else {
            // Regular file
            if (hasTypeDefinition(STRUCT, type, search)) {
                // Found it
                return search;
            }
            else if (hasTypeDefinition(CLASS, type, search)) {
                // Found it
                return search;
            }
            else if (hasTypeDefinition(ENUM, type, search)) {
                // Found it
                return search;
            }
        }
    }

    // Couldn't find it
    free(search);
    return NULL;
}

bool Utils::hasTypeDefinition(data_type_t dt, char * type, char * filename) {
    char buf[BUF_SIZE];
    char DELIM[] = " \t\n\v\f\r";
    char * data_type;
    char ** all_tokens;
    char * token;

    int offset;
    int allocated_tokens;

    bool found = false;
    FILE * fp;

    strncpy(buf, type, BUF_SIZE);

    // printf("Searching file %s\n", filename);
    
    switch (dt) {
        case STRUCT:
            data_type = (char *)malloc(sizeof(char) * 7);
            memset(data_type, 0, 7);
            strcpy(data_type, "struct");
            break;
        case CLASS:
            data_type = (char *)malloc(sizeof(char) * 6);
            memset(data_type, 0, 6);
            strcpy(data_type, "class");
            break;
        case ENUM:
            data_type = (char *)malloc(sizeof(char) * 5);
            memset(data_type, 0, 5);
            strcpy(data_type, "enum");
            break;
    }

    //printf("Searching for %s %s\n", data_type, type);

    if ((fp = fopen(filename, "r")) == NULL) {
        perror("Could not open file");
    }

    all_tokens = (char **)malloc(BUF_SIZE * sizeof(char *));
    allocated_tokens = BUF_SIZE;

    offset = 0;
    while (fgets(buf, BUF_SIZE, fp)) {
        token = strtok(buf, DELIM);

        for (; token != NULL; token = strtok(NULL, DELIM), offset++) {
            if (offset == allocated_tokens) {
                allocated_tokens += BUF_SIZE;
                all_tokens = (char **)realloc(all_tokens, allocated_tokens * sizeof(char *));
            }

            all_tokens[offset] = (char *)malloc(strlen(token) + 1);
            strncpy(all_tokens[offset], token, strlen(token) + 1);
        }
    }

    // At this point, we have all of the tokens in 'all_tokens' - search for the
    // type followed by a '{'
    
    for (int i = 0; i < offset - 2; ++i) {
        if (!strcmp(all_tokens[i], data_type)) {
            if (!strcmp(all_tokens[i + 1], type)) {
                //if (!strncmp(all_tokens[i + 2], "{", 1)) {
                if (!strcmp(all_tokens[i + 2], "{")) {
                    // Found it!
                    found = true;
                    break;
                }
            }
            else if (!strcmp(all_tokens[i + 1] + strlen(all_tokens[i + 1]) - 1, "{")) {
                // Last character is a '{'
                if (!strncmp(all_tokens[i + 1], type, strlen(type))) {
                    // Found it!
                    found = true;
                    break;
                }
            }
        }
    }


    // Clean up
    free(data_type);
    for (int i = 0; i < offset; i++) {
        free(all_tokens[i]);
    }
    free(all_tokens);
    fclose(fp);

    return found;
}
