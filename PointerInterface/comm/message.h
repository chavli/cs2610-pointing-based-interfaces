#ifndef _MESSAGE_H
#define _MESSAGE_H

#include <stdint.h>
#include <string>

typedef enum{
  NONE,
  DRAW_RECT,
  DRAW_ELLIPSE,
  DRAW_TEXT,
  SET_DIM,
  CURSOR_POS,
  CURSOR_SEL,
  DUAL_POS,
  DUAL_SEL,
  LH_SWIPE
} message_type_t;

typedef struct{
  float x1, y1;
  float x2, y2;
  float r, g, b;
} glshape_data_t;

typedef struct{
  float x1, y1;
  float r, g, b;
  char s[512];
} gltext_data_t;

typedef glshape_data_t gldimension_t;
typedef gltext_data_t glposition_t;


typedef struct{
  message_type_t type;
  
  //the frame that generated this message
  uint32_t fid;

  //pointers to the data associated with this message
  void *p_data;
  void **dp_data;

} message_t;

static std::string messageTypeToString(message_type_t t){
  std::string s;
  switch(t){
    case NONE:
      s = "NONE";
      break;
    case DRAW_RECT:
      s = "DRAW_RECT";
      break;
    case DRAW_ELLIPSE:
      s = "DRAW_ELLIPSE";
      break;
    case DRAW_TEXT:
      s = "DRAW_TEXT";
      break;
    case SET_DIM:
      s = "SET_DIMENSION";
      break;
    case CURSOR_POS:
      s = "CURSOR_POSITON";
      break;
    case CURSOR_SEL:
      s = "CURSOR_SELECT";
      break;
    default:
      s = "UNKNOWN";
      break;
  }

  return s;
}


#endif  //_MESSAGE_H



