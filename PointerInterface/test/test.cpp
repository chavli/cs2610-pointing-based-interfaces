#include <GL/glui.h>
#include "../comm/message.h"

#include <iostream>
#include <fstream>
#include <list>
#include <cassert>

using namespace std;


// OpenGL stuff
#define W_WIDTH 600
#define W_HEIGHT 400
#define TIMER 50

gldimension_t kinect_dims;
list<glposition_t> data_points;
int main_window;
int render_x;
int render_y;

// GL callbacks and such
void init(void);
void display(void);
void motion(int x, int y);
void timer(int);
void glui_control(int);

// Mapper
void map_kinect_point(int x_loc, int y_loc, int * mapped_x, int * mapped_y);

// Goal of this program is to map things correctly
// Draw a really simple glui window, eveyr TIMER secs move the mouse 
// to the next position
int main(int argc, char ** argv) {
    ifstream ins;
    string filename;
    glposition_t mouse_loc;

    if (argc != 2) {
        cerr << "Usage: " << *argv << " <movement_file>" << endl;
        return -1;
    }

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(W_WIDTH, W_HEIGHT);
    glutInitWindowPosition(0, 0);
    main_window = glutCreateWindow("TEST");

    // GLUT Callbacks
    glutDisplayFunc(display);
    glutPassiveMotionFunc(motion);
    init();

    filename = *(++argv);
    ins.open(filename.c_str());
    if (!ins.is_open()) {
        cerr << "Could not open file " << filename << endl;
        return -1;
    }

    ins >> kinect_dims.x2 >> kinect_dims.y1 >>
        kinect_dims.x1 >> kinect_dims.y2;

    cerr << "Kinect dims (x): " << kinect_dims.x1 << " to " << kinect_dims.x2 << endl;
    cerr << "Kinect dims (y): " << kinect_dims.y2 << " to " << kinect_dims.y1 << endl;

    while (ins >> mouse_loc.x1 >> mouse_loc.y1) {
        // New point
        data_points.push_back(mouse_loc);
    }

    ins.close();

    glutTimerFunc(TIMER, timer, 0);
    glutMainLoop();
    return 0;
}


void init(void) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0, W_WIDTH, 0.0, W_HEIGHT, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0.85, 0.85, 0.85, 1.0);

    render_x = 0;
    render_y = 0;
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0, 0.0, 0.0);
    
    glBegin(GL_POLYGON);
        glVertex2f(render_x, render_y);
        glVertex2f(render_x + 10, render_y);
        glVertex2f(render_x + 10, render_y + 10);
        glVertex2f(render_x, render_y + 10);
    glEnd();
    
    glutWarpPointer(render_x, W_HEIGHT - render_y);
    glutSwapBuffers();
}

void motion(int x, int y) {
}

void timer(int id) {
    glposition_t loc;

    // "Move" mouse
    loc = data_points.front();
    data_points.pop_front();

    // glutWarpPointer(loc.x1, loc.y1);
    map_kinect_point(loc.x1, loc.y1, &render_x, &render_y);
    
    cerr << "x: " << render_x << " y:" << render_y << endl;

    // Reset timer, post redisplay
    if (!data_points.empty()) {
        glutTimerFunc(TIMER, timer, 0);
    }

    glutPostRedisplay();
}

void glui_control(int id) {
    switch (id) {
        case 0:

        default:
            break;
    }
}

void map_kinect_point(int x_loc, int y_loc, int * mapped_x, int * mapped_y) {
    float x_map;
    float y_map;

    x_map = (float)(x_loc - kinect_dims.x1) / 
            (float)(kinect_dims.x2 - kinect_dims.x1);

    y_map = (float)(y_loc - kinect_dims.y2) / 
            (float)(kinect_dims.y1 - kinect_dims.y2);

    if ((x_loc < kinect_dims.x1 || x_loc > kinect_dims.x2) ||
        (y_loc < kinect_dims.y2 || y_loc > kinect_dims.y1)) {
        // Invalid point - out of the kinect dimensions
        *mapped_x = 0;
        *mapped_y = 0;
        return;
    }

    if (!(x_map > 0 && y_map > 0 && x_map < 1 && y_map < 1)) {
        cerr << "INVALID MAPPING" << endl;
        cerr << "x_loc: " << x_loc << ", y_loc: " << y_loc << endl;
        cerr << "x_map: " << x_map << ", y_map: " << y_map << endl;
        assert(1 == 0);
    }
    cerr << "x_map: " << x_map << endl;
    cerr << "y_map: " << y_map << endl;

    *mapped_x = x_map * W_WIDTH;
    *mapped_y = y_map * W_HEIGHT;

    cerr << "Mapping " << x_loc << ", " << y_loc << " to " <<
        *mapped_x << ", " << *mapped_y << endl;
}
