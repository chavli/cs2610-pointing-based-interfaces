﻿#include "stdafx.h"
#include "HandTracker.h"
#include "resource.h"
#include <math.h>
#include <strsafe.h>

static const float g_JointThickness = 3.0f;
static const float g_TrackedBoneThickness = 6.0f;
static const float g_InferredBoneThickness = 1.0f;

INT32 distance(D2D_POINT_2F a, D2D_POINT_2F b);

/******************************************************************************
*	general management
******************************************************************************/

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow){
    HandTracker application;
    application.run(hInstance, nCmdShow);
}

HandTracker::HandTracker() :
    d2d_factory(NULL),
    skel_event_handle(INVALID_HANDLE_VALUE),
	color_event_handle(INVALID_HANDLE_VALUE),
    color_stream_handle(INVALID_HANDLE_VALUE),
    seated(false),
    target_renderer(NULL),
    tracked_joint_color(NULL),
    inferred_joint_color(NULL),
    tracked_bone_color(NULL),
    inferred_bone_color(NULL),
    kinect_sensor(NULL)
{
    ZeroMemory(skel_points,sizeof(skel_points));
}

HandTracker::~HandTracker(){
    if (kinect_sensor)
        kinect_sensor->NuiShutdown();

    if (skel_event_handle && (skel_event_handle != INVALID_HANDLE_VALUE))
        CloseHandle(skel_event_handle);

    freeD2DResources();
    release(d2d_factory);
    release(kinect_sensor);
}

/******************************************************************************
*	D2D management
******************************************************************************/
HRESULT HandTracker::allocD2DResources(){
    HRESULT hr = S_OK;

    // If there isn't currently a render target, we need to create one
    if (NULL == target_renderer){
        RECT rc;
        GetWindowRect( GetDlgItem( mwindow, IDC_VIDEOVIEW ), &rc );  
    
        int width = rc.right - rc.left;
        int height = rc.bottom - rc.top;
        D2D1_SIZE_U size = D2D1::SizeU( width, height );
        D2D1_RENDER_TARGET_PROPERTIES rtProps = D2D1::RenderTargetProperties();
        rtProps.pixelFormat = D2D1::PixelFormat( DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE);
        rtProps.usage = D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE;

        // Create a Hwnd render target, in order to render to the window set in initialize
        hr = d2d_factory->CreateHwndRenderTarget(
            rtProps,
            D2D1::HwndRenderTargetProperties(GetDlgItem( mwindow, IDC_VIDEOVIEW), size),
            &target_renderer
            );
        if ( FAILED(hr) ){
            setStatusMessage(L"Couldn't create Direct2D render target!");
            return hr;
        }

        target_renderer->CreateSolidColorBrush(D2D1::ColorF(68, 192, 68), &tracked_joint_color); //light green
        target_renderer->CreateSolidColorBrush(D2D1::ColorF(255, 255, 0), &inferred_joint_color);	//yellow
        target_renderer->CreateSolidColorBrush(D2D1::ColorF(0, 128, 0), &tracked_bone_color);	//green
        target_renderer->CreateSolidColorBrush(D2D1::ColorF(128, 128, 128), &inferred_bone_color);	//gray
		target_renderer->CreateSolidColorBrush(D2D1::ColorF(0,0, 255), &fill_blue);	
		target_renderer->CreateSolidColorBrush(D2D1::ColorF(255, 0, 0), &fill_red);
		target_renderer->CreateSolidColorBrush(D2D1::ColorF(0, 255, 0), &fill_green);

		createTargets();
    }

    return hr;
}

void HandTracker::freeD2DResources( ){
    release(target_renderer);
    release(tracked_joint_color);
    release(inferred_joint_color);
    release(tracked_bone_color);
    release(inferred_joint_color);
}

/******************************************************************************
*	program control functions
******************************************************************************/
int HandTracker::run(HINSTANCE instance, int mode){
    MSG       msg = {0};
    WNDCLASS  wc  = {0};

    // Dialog custom window class
    wc.style         = CS_HREDRAW | CS_VREDRAW;
    wc.cbWndExtra    = DLGWINDOWEXTRA;
    wc.hInstance     = instance;
    wc.hCursor       = LoadCursorW(NULL, IDC_ARROW);
    wc.hIcon         = LoadIconW(instance, MAKEINTRESOURCE(IDI_APP));
    wc.lpfnWndProc   = DefDlgProcW;
    wc.lpszClassName = L"HandTrackerAppDlgWndClass";
	

    if (!RegisterClassW(&wc))
        return 0;

    // Create main application window
    HWND new_window = CreateDialogParamW(
        instance,
		MAKEINTRESOURCE(IDD_APP),
        NULL,
        (DLGPROC)HandTracker::messageRouter, 
        reinterpret_cast<LPARAM>(this));

    ShowWindow(new_window, mode);

    const int num_events = 2;
    HANDLE event_handlers[num_events];
	event_handlers[0] = skel_event_handle;
    event_handlers[1] = color_event_handle;

	//NOTE: main execution loop
    while (WM_QUIT != msg.message){
        // check to see if we have either a message or a Kinect event
        DWORD dwEvent = MsgWaitForMultipleObjects(num_events, event_handlers, FALSE, INFINITE, QS_ALLINPUT);
		
        // Check if this is an event we're waiting on and not a timeout or message
        if (WAIT_OBJECT_0 == dwEvent)
			update();

        if (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE)){
            if ((new_window != NULL) && IsDialogMessageW(new_window, &msg))
                continue;

            TranslateMessage(&msg);
            DispatchMessageW(&msg);
        }
    }
    return static_cast<int>(msg.wParam);
}

void HandTracker::update(){
    if (NULL == kinect_sensor)
        return;

    // quickly test if it is time to process a skeleton
	if ( WAIT_OBJECT_0 == WaitForSingleObject(color_event_handle, 0) )
		processColorFrame();
    if ( WAIT_OBJECT_0 == WaitForSingleObject(skel_event_handle, 0) )
		processSkeleton();

}

HRESULT HandTracker::connect(){
    INuiSensor * cur_sensor;
    int num_sensors = 0;
    HRESULT hr = NuiGetSensorCount(&num_sensors);

    if (FAILED(hr))
        return hr;

    // check all connected kinect's and go with the first one that works
    for (int i = 0; i < num_sensors; ++i) {
        // Create the sensor so we can check status, if we can't create it, move on to the next
        hr = NuiCreateSensorByIndex(i, &cur_sensor);
        if (FAILED(hr))
            continue;

        // Get the status of the sensor, and if connected, then we can initialize it
        hr = cur_sensor->NuiStatus();
        if (S_OK == hr){
            kinect_sensor = cur_sensor;
            break;
        }

        // This sensor wasn't OK, so release it since we're not using it
        cur_sensor->Release();
    }

    if (NULL != kinect_sensor){
        // Initialize the Kinect and specify that we'll be using skeleton
        hr = kinect_sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR); 
        if (SUCCEEDED(hr)){
            // Create an event that will be signaled when skeleton data is available
            skel_event_handle = CreateEventW(NULL, TRUE, FALSE, NULL);
			color_event_handle = CreateEventW(NULL, TRUE, FALSE, NULL);

            // Open a skeleton stream to receive skeleton data
            hr = kinect_sensor->NuiSkeletonTrackingEnable(skel_event_handle, 0); 
			if( FAILED(hr ) ) setStatusMessage(L"No Skeleton Camera");

			hr = kinect_sensor->NuiImageStreamOpen(
                NUI_IMAGE_TYPE_COLOR,
                NUI_IMAGE_RESOLUTION_640x480,
                0,
                2,
                color_event_handle,
                &color_stream_handle);

			if( FAILED(hr ) ) setStatusMessage(L"No Color Camera");
        }
    }

    if (NULL == kinect_sensor){
        setStatusMessage(L"No ready Kinect found!");
        return E_FAIL;
    }

	hr = allocD2DResources( );

    return hr;
}

/******************************************************************************
*	data processing
******************************************************************************/

void HandTracker::processColorFrame(){
	HRESULT hr;
	NUI_IMAGE_FRAME frame;
	hr = kinect_sensor->NuiImageStreamGetNextFrame(color_stream_handle, 0, &frame);
	
	INuiFrameTexture * texture = frame.pFrameTexture;
    NUI_LOCKED_RECT locked_rect;
	texture->LockRect(0, &locked_rect, NULL, 0);
	if (locked_rect.Pitch != 0)
		image_renderer->Draw(static_cast<BYTE *>(locked_rect.pBits), locked_rect.size);

	texture->UnlockRect(0);
    kinect_sensor->NuiImageStreamReleaseFrame(color_stream_handle, &frame);
}

void HandTracker::processSkeleton(){
	HRESULT hr;

	//process skeleton data
    NUI_SKELETON_FRAME skeletonFrame = {0};

    hr = kinect_sensor->NuiSkeletonGetNextFrame(0, &skeletonFrame);
    if ( FAILED(hr) )
        return;

    // smooth out the skeleton data
    kinect_sensor->NuiTransformSmooth(&skeletonFrame, NULL);

    target_renderer->BeginDraw();
    target_renderer->Clear( );
    
    RECT rct;
    GetClientRect( GetDlgItem( mwindow, IDC_VIDEOVIEW ), &rct);
    int width = rct.right;
    int height = rct.bottom;

    for (int i = 0 ; i < NUI_SKELETON_COUNT; ++i){
        NUI_SKELETON_TRACKING_STATE trackingState = skeletonFrame.SkeletonData[i].eTrackingState;

        // We're tracking the skeleton, draw it
        if (NUI_SKELETON_TRACKED == trackingState){
            drawSkeleton(skeletonFrame.SkeletonData[i], width, height);
			calculateCollision(skeletonFrame.SkeletonData[i]);
		}
		// we've only received the center point of the skeleton, draw that
        else if (NUI_SKELETON_POSITION_ONLY == trackingState){
            D2D1_ELLIPSE ellipse = D2D1::Ellipse(
                skeletonToScreen(skeletonFrame.SkeletonData[i].Position, width, height),
                g_JointThickness,
                g_JointThickness
                );

            target_renderer->DrawEllipse(ellipse, tracked_joint_color);
        }
    }

    hr = target_renderer->EndDraw();

    // device lost, need to recreate the render target
    if (D2DERR_RECREATE_TARGET == hr){
        hr = S_OK;
        freeD2DResources();
    }
}

/******************************************************************************
*	window event handlers
******************************************************************************/
LRESULT CALLBACK HandTracker::messageRouter(HWND hwnd,
											UINT type,
											WPARAM wparam,
											LPARAM lparam){
    HandTracker* pThis = NULL;
    
    if ( type == WM_INITDIALOG ){
        pThis = reinterpret_cast<HandTracker*>(lparam);
        SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis));
    }
    else
        pThis = reinterpret_cast<HandTracker*>(::GetWindowLongPtr(hwnd, GWLP_USERDATA));

    if (pThis)
        return pThis->processMessage(hwnd, type, wparam, lparam);

    return 0;
}

LRESULT CALLBACK HandTracker::processMessage(HWND hwnd, 
											 UINT type, 
											 WPARAM wparam, 
											 LPARAM lparam){
    switch (type){
        case WM_INITDIALOG:{	//when the app first starts
				mwindow = hwnd;
				D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2d_factory);

				image_renderer = new ImageRenderer();
				HRESULT hr = image_renderer->Initialize(GetDlgItem(mwindow, IDC_REALVIEW),
					d2d_factory, color_width, color_height, color_width * sizeof(long));

				connect();	//look for the sensor
			}
	        break;
        case WM_CLOSE:	//when X is clicked to close
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:	//force quit?
            PostQuitMessage(0);
            break;

        case WM_COMMAND:	//when some button event happens
            // If it was for the near mode control and a clicked event, change near mode
            if (IDC_CHECK_SEATED == LOWORD(wparam) && BN_CLICKED == HIWORD(wparam)){
                seated = !seated;

                if (NULL != kinect_sensor)
                    kinect_sensor->NuiSkeletonTrackingEnable(skel_event_handle, seated ? NUI_SKELETON_TRACKING_FLAG_ENABLE_SEATED_SUPPORT : 0);
            }
            break;
    }
    return FALSE;
}

/******************************************************************************
*	graphics functions
******************************************************************************/

void HandTracker::createTargets(){
	D2D_POINT_2F loc = {850, 800};
	D2D_POINT_2F loc2 = {350, 100};

	targets[0] = D2D1::Ellipse(loc, 50, 50);
	targets[1] = D2D1::Ellipse(loc2, 50, 50);
}

void HandTracker::calculateCollision(const NUI_SKELETON_DATA & skel){

	target_renderer->FillEllipse(targets[0], fill_red);
	target_renderer->FillEllipse(targets[1], fill_red);

	int left_hand, right_hand;
	for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i){
		if( i == NUI_SKELETON_POSITION_HAND_RIGHT )
			right_hand = i;
		else if( i == NUI_SKELETON_POSITION_HAND_LEFT )
			left_hand = i;
	}
	
	//touch a circle
	if( distance(skel_points[left_hand], targets[0].point) <= 50 || distance(skel_points[right_hand], targets[0].point) <= 50){
		target_renderer->FillEllipse(targets[0], fill_green);
	}
	if( distance(skel_points[left_hand], targets[1].point) <= 50 || distance(skel_points[right_hand], targets[1].point) <= 50){
		target_renderer->FillEllipse(targets[1], fill_green);
	}

	//grab a circle 
	if( distance(skel_points[left_hand], targets[0].point) <= 90 && distance(skel_points[right_hand], targets[0].point) <= 90){
		
		D2D_POINT_2F move = {(skel_points[left_hand].x + skel_points[right_hand].x)/2
							, (skel_points[left_hand].y + skel_points[right_hand].y)/2};
		targets[0].point = move;
		target_renderer->FillEllipse(targets[0], fill_blue);
	}

	if( distance(skel_points[left_hand], targets[1].point) <= 90 && distance(skel_points[right_hand], targets[1].point) <= 90){
		
		D2D_POINT_2F move = {(skel_points[left_hand].x + skel_points[right_hand].x)/2
							, (skel_points[left_hand].y + skel_points[right_hand].y)/2};
		targets[1].point = move;
		target_renderer->FillEllipse(targets[1], fill_blue);
	}


	target_renderer->DrawEllipse(targets[0], inferred_joint_color);
	target_renderer->DrawEllipse(targets[1], inferred_joint_color);
}

void HandTracker::drawSkeleton(const NUI_SKELETON_DATA & skel,
							   int width,
							   int height){

    for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i)
	   skel_points[i] = skeletonToScreen(skel.SkeletonPositions[i], width, height);

    // Left Arm
    drawBone(skel, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT);
    drawBone(skel, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT);
    drawBone(skel, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);

    // Right Arm
    drawBone(skel, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT);
    drawBone(skel, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT);
    drawBone(skel, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);

    // Draw the joints in a different color
    for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i){
        D2D1_ELLIPSE ellipse; 
		
		//make the wrist joints bigger
		if( i == NUI_SKELETON_POSITION_HAND_RIGHT || i == NUI_SKELETON_POSITION_HAND_LEFT)
			ellipse = D2D1::Ellipse( skel_points[i], 5*g_JointThickness, 5*g_JointThickness );
		else
			ellipse = D2D1::Ellipse( skel_points[i], g_JointThickness, g_JointThickness );
		target_renderer->FillEllipse(ellipse, fill_blue);

        if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_INFERRED )
            target_renderer->DrawEllipse(ellipse, inferred_joint_color);
        else if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_TRACKED )
            target_renderer->DrawEllipse(ellipse, tracked_joint_color);
    }
}

void HandTracker::drawBone(const NUI_SKELETON_DATA & skel, 
						   NUI_SKELETON_POSITION_INDEX start, 
						   NUI_SKELETON_POSITION_INDEX end){

    NUI_SKELETON_POSITION_TRACKING_STATE start_state = skel.eSkeletonPositionTrackingState[start];
    NUI_SKELETON_POSITION_TRACKING_STATE end_state = skel.eSkeletonPositionTrackingState[end];

    if (start_state == NUI_SKELETON_POSITION_NOT_TRACKED || end_state == NUI_SKELETON_POSITION_NOT_TRACKED)
        return;
    
    // Don't draw if both points are inferred
    if (start_state == NUI_SKELETON_POSITION_INFERRED && end_state == NUI_SKELETON_POSITION_INFERRED)
        return;

    // We assume all drawn bones are inferred unless BOTH joints are tracked
    if (start_state == NUI_SKELETON_POSITION_TRACKED && end_state == NUI_SKELETON_POSITION_TRACKED)
        target_renderer->DrawLine(skel_points[start], skel_points[end], tracked_bone_color, g_TrackedBoneThickness);
    else
        target_renderer->DrawLine(skel_points[start], skel_points[end], tracked_bone_color, g_InferredBoneThickness);
}

//This is where we have to map movements to screen
D2D1_POINT_2F HandTracker::skeletonToScreen(Vector4 point, int width, int height){
    LONG x, y;
    USHORT depth;

    // Calculate the skeleton's position on the screen
    // NuiTransformSkeletonToDepthImage returns coordinates in NUI_IMAGE_RESOLUTION_320x240 space
    NuiTransformSkeletonToDepthImage(point, &x, &y, &depth);

    float screenPointX = static_cast<float>(x * width) / screen_width;
    float screenPointY = static_cast<float>(y * height) / screen_height;

    return D2D1::Point2F(screenPointX, screenPointY);
}

/******************************************************************************
*	miscellaneous
******************************************************************************/

void HandTracker::setStatusMessage(WCHAR * szMessage){
    SendDlgItemMessageW(mwindow, IDC_STATUS, WM_SETTEXT, 0, (LPARAM)szMessage);
}

INT32 distance(D2D_POINT_2F a, D2D_POINT_2F b){
	return sqrt( pow(b.x - a.x, 2) + pow(b.y - a.y, 2) );
}












