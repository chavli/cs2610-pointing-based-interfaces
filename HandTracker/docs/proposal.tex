\documentclass[12pt]{article}

\usepackage[fleqn]{amsmath}

\usepackage{amsfonts}

\usepackage{algorithm}

\usepackage{algorithmic}

\usepackage{amsthm}

\usepackage[hmargin=1cm,vmargin=1cm]{geometry}
%\usepackage{graphicx} %allows use of figures

%\pagestyle{empty} %<== DELETE IF YOUR TEST HAS MORE THAN ONE PAGE

% (this page style suppresses page numbers.)

%Adjust the margins:


%puts in a horizontal rule the width of the text with space

%before and after.

\newcommand{\sep}{\ifhmode\par\fi\vskip6pt\hrule\vskip6pt}

\newcounter{rcounter}

%A new environment for a test problem.

\newcounter{problem}

\newenvironment{problem}

{

\stepcounter{problem}%

\filbreak%

\sep

\noindent\arabic{problem}.

}

{

\filbreak

}

\begin{document}
{
\raggedright
\textsc{Cha Li, Brian Kocoloski \\
\footnotesize{\{chavli, briankoco\}@cs.pitt.edu} \hfill \footnotesize {CS2610: Human Computer Interaction}\\
\vspace{.05in}
\footnotesize {
Final Project Proposal\\
31 Oct 2012
}}}

\pagestyle{empty} %remove page numbers
\vspace{.15in}
\hrule
\vspace{.2in}

%
% start typing from here
%

\section{Research Question}
Can natural user interfaces (NUIs) improve the ability of programmers to build
accurate mental models of sufficiently large code bases?

\section{Introduction}
Understanding and building accurate mental models of large code bases is a
difficult task for even highly experienced programmers. The effect of this
learning curve is perhaps most pronounced when large software companies hire
young developers from universities. While these programmers may have sufficient
experience in the required programming languages, understanding the structure of
one or more large code bases that they may be required to work on can be a
rather daunting task.\\

Contemporary integrated development environments (IDEs) use a file hierarchy
approach to manage and display files within a project. This system does a very
good job of showing how the project is organized, however, it loses much of the
semantic information such as file dependencies. Additionally, most IDEs only have one layer of visual
abstraction; you either see the whole file or you see nothing. Some IDEs allow
sections of code (within a file) to be "collapsed" so some code can be hidden
away. The problem with this is that the user still has to scroll linearly
through a file to see all the collapsed sections to explore them.  We believe
that the task of displaying code and representing a development project can be
better achieved using a graph (nodes + edges) representation, and that
navigation can be done in a non-linear fashion by allowing the user to move
through the display space in a much more fluid manner.\\

We seek to investigate ways for programmers to build accurate mental models of
large code bases in less time than current keyboard and mouse technologies
allow. Specifically, we plan to investigate techniques that will allow
programmers to quickly build accurate mental modelsof code
bases by using natural user interfaces. We seek to augment code review sessions,
such as pair programming sessions or informal walkthroughs, with natural user
interfaces so that programmers may more easily navigate through code bases.

\section{Related Work}
We provide a brief analysis of related work by examining research in both code
review and pair programming.

\paragraph{Code Review}
Much research has been performed in the code review domain. For example,
researchers have looked at improving the overall code review process with the
"control chart"~\cite{jacob} and turning code review into a team
exercise~\cite{trytten}. There are undoubtedly many other works. While these
techniques have had varying degrees of success, all have assumed the presence of
more standard user interfaces. We believe that visualizing code bases will help users 
quickly understand the structure of the project. Accordingly, we note that our approach does not conflict with the
approaches taken in these projects. We are not suggesting new techniques for
performing code review as much as we are suggesting a new interface that can
augment the techniques presented in these projects.

\paragraph{Pair Programming}
Pair programming is one example of how users may perform code review. In pair
programming, typically one user sits at a computer terminal, operating a mouse
and keyboard, while another user watches them, giving directions about what
should be coded. This allows the user operating the machine to focus strictly on
converting the concepts into code. Researchers have previously demonstrated
the benefits of pair programming~\cite{williams}, particularly as it pertains to
introductory Computer Science courses~\cite{mcdowell, nagappan}. \\

We believe that similar benefits can be had for those that may be debugging
code, with a slight modification. In particular, one user may assume the task of
machine operator, but instead of the other user simply watching them, this
person may instead make use of a separate natural interface in which he/she can
more fluidly navigate the code base and find the problems causing the incorrect
behavior.

\section{Hypothesis}
We believe that a natural user interface is a very appropriate tool for
understanding large, unfamiliar code bases. We believe that programmers of
varied experience levels will build more accurate mental models of a large code
base using our natural interface than they will when using a simply keyboard and
mouse interface.

\section{Method}
\paragraph{Design Study}
Our implementation will use a Microsoft Kinect camera along with the Microsoft
Kinect SDK to develop software to track bodies and hand gestures. These gestures
will then provide the input to our NUI. Additionally, we will design a virtual
environment that visualizes a code project and allows the user to move through
it using body motions. A projector/large screen will be used to display the
environment so that multiple users will be able to collaborate.

\paragraph{Evaluation}
We plan to evaluate our NUI for code visualization by loading a set of large
code bases, and allowing different users to explore the code. More specifically,
we plan to present users with two different types of tasks: (1) independently
review a code base and answer questions about the organization and file
dependencies, and (2) participate in a pair programming session with the goal of
finding and fixing a small set of bugs.\\

Our system will be compared to contemporary IDEs such as Eclipse or Microsoft's
Visual Studio, and performance will be based on how quickly the user can
understand the code. We will measure how long it takes for a user to gain a
basic understanding of data flow and dependencies, and how long it takes for the
users to find and fix bugs.\\

Particularly in the case of the collaborative coding experiments, we will have
to be careful to make sure that users do not show improvements in understanding
the code bases due to previous exposure. Thus, we plan to present two separate
code bases to each coding team, one for each interface. Then, our analysis will
not compare the individual improvements within groups, but rather the
performance differences for each interface among all the groups. We note that in
order for this technique to be valid, we will need to assign an equal number of
expert and novice programmers to each code base (see the study recruitment
section for more details).

\paragraph{Ecological Validity}
The goal of our project is to design a code visualization and interface system
that allows users to quickly orient themselves to an unknown code project. We
believe our study achieves this goal as it presents to the test subjects a large
code base with which they are unfamiliar, in both the framework of a traditional
IDE as well as with our NUI.

\section{Study Recruitment Plan}
We want to focus strictly on how long it takes for users to understand the code
base and not how long it takes for them to learn the underlying programming
language. Given this requirement, our recruitment plan is to get testers from
the CS department that are familiar with the language that we present. As for
amount of programming experience, we want to recruit a good range (as long as
the previous condition is met) so that we can get feedback from experienced
developers as well as novice developers. Ideally, our tester pool would contain
both undergraduate and graduate students studying CS (or anyone with programming
experience). 

\section{Biggest Risk}
The hardest part of this project will most likely be getting the video
recognition software and algorithms working with the Microsoft Kinect. Both of
us have never programmed one so this first obstacle will be to understand how it
works and how to write code for it. Getting the Kinect to work is definitely the
most important part since without it, this idea won't work. Another difficult
part, although not a risk, is picking the library to do the visualization.
OpenGL is one option, but the choice has to be powerful enough to create the
bubble graph and at the same time not so low level that we have to reinvent a
lot of things.

\paragraph{Idea Weaknesses}
A text editor interface is (this is probably safe to say) the best for
developing large software projects where direct control over code is required.
Some IDEs allow users to put together puzzle pieces or something else to build
code, but these are limited to specific domains or toy projects. Our project
does not attempt improve the code development process but focuses more on
improving the post-development stages such as code reviews and general
maintenance. Ideally, our system would help a new programmer quickly build an
accurate mental model of the project.\\

Another possible weakness of our system is related to debugging. For serious
problems, a traditional debugger is the best as it provides all the relevant
information in a simple and concise manner. Memory dumps and stack traces are
probably better represented in text than in an overly fancy graphical method.
However, as stated earlier, our system aims to help the programmer build a
better mental model of the code which indirectly improves the debugging
process.

\bibliographystyle{acm}
\bibliography{proposal}
\end{document}
