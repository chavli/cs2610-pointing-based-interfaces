﻿#ifndef _HANDTRACKER_H
#define _HANDTRACKER_H

#pragma once

#include "resource.h"
#include "NuiApi.h"
#include "ImageRenderer.h"

class HandTracker{
	//window dimensions for skeleton
	static const int screen_width = 320;
	static const int screen_height = 240;

	//window dimensions for live feed
	static const int color_width = 640;
	static const int color_height = 380;

	static const int        cStatusMessageMaxLen = MAX_PATH*2;
public:
	HandTracker();
	~HandTracker();

	//entry point for the code
	int	run(HINSTANCE, int);

	//window event message handler
	static LRESULT CALLBACK messageRouter(HWND, UINT, WPARAM, LPARAM);
	LRESULT CALLBACK processMessage(HWND, UINT,	WPARAM,	LPARAM);

private:
	HWND mwindow;	//main app window
	bool seated;

	//the kinect sensor
	INuiSensor *kinect_sensor;

	//all the points on the body the kinect tracks and other D2D things
    ID2D1HwndRenderTarget *target_renderer;
	ImageRenderer	*image_renderer;

    ID2D1SolidColorBrush *tracked_joint_color;
    ID2D1SolidColorBrush *inferred_joint_color;
    ID2D1SolidColorBrush *tracked_bone_color;
    ID2D1SolidColorBrush *inferred_bone_color;

	ID2D1SolidColorBrush *fill_blue;
	ID2D1SolidColorBrush *fill_red;
	ID2D1SolidColorBrush *fill_green;

	D2D1_ELLIPSE targets[2];
	
	D2D1_POINT_2F skel_points[NUI_SKELETON_POSITION_COUNT];
	ID2D1Factory *d2d_factory;

	//handle (typedef for void*, so maybe a function pointer) 
	HANDLE color_stream_handle;

	//handle for skeleton events
	HANDLE skel_event_handle;
	HANDLE color_event_handle;

	//the main execution loop/function
	void update(void);

	//connect to sensor
	HRESULT connect(void);

	//process new data from sensor
	void processSkeleton(void);
	void processColorFrame(void);

	//handle resources allocated to D2D
	HRESULT allocD2DResources(void);
	void freeD2DResources(void);


	//graphics methods
	void createTargets(void);

	void drawBone(const NUI_SKELETON_DATA &, 
		NUI_SKELETON_POSITION_INDEX, 
		NUI_SKELETON_POSITION_INDEX
	);

	void drawSkeleton(const NUI_SKELETON_DATA &, int, int);
    void calculateCollision(const NUI_SKELETON_DATA &);
	D2D1_POINT_2F skeletonToScreen(Vector4, int, int);

	void setStatusMessage(WCHAR * szMessage);
};




#endif